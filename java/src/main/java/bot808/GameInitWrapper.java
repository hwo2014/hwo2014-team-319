package bot808;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;

class Race {
    protected Track track;
    protected ArrayList<Car> cars;
    protected RaceSession raceSession;

    Race(Track track, ArrayList<Car> cars, RaceSession raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }

    @Override
    public String toString() {
        String ret = "track:\n" + track.toString() + "\n";
        ret += "cars:[\n";
        for (Car car : cars) {
            ret += "    " + car.toString() + "\n";
        }
        ret += "]\n";
        ret += "raceSession: " + raceSession.toString() + "\n}\n";
        return ret;
    }
}

class Car {
    protected CarId id;
    protected Dimensions dimensions;

    Car(CarId id, Dimensions dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }

    @Override
    public String toString() {
        return "{" + id.toString() + ", " + dimensions.toString() + "}";
    }
}

class CarId {
    protected String name;
    protected String color;

    CarId(String name, String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public String toString() {
        return "{" + name + ", " + color + "}";
    }

    @Override
    protected CarId clone() throws CloneNotSupportedException {
        return new CarId(name, color);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        CarId o = (CarId)obj;
        return name.equals(o.name) && color.equals(o.color);
    }
}

class Dimensions {
    protected double length;
    protected double width;
    protected double guideFlagPosition;

    Dimensions(double length, double width, double guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }

    @Override
    public String toString() {
        return  "{ length = " + length + ", width = " + width + ", " + guideFlagPosition + "}";
    }
}

class Track {
    protected String id;
    protected String name;
    protected ArrayList<Piece> pieces;
    protected ArrayList<Lane> lanes;
    protected StartingPoint startingPoint;

    Track(String id, String name, ArrayList<Piece> pieces, ArrayList<Lane> lanes, StartingPoint startingPoint) {
        this.id = id;
        this.name = name;
        this.pieces = (ArrayList<Piece>) pieces.clone();
        this.lanes = (ArrayList<Lane>) lanes.clone();
        Collections.sort(this.lanes);
        this.startingPoint = startingPoint;
    }

    @Override
    public String toString() {
        String ret = "{\n";
        ret += "id = " + id + ", name = " + name + "\n";
        ret += "pieces:[\n";
        for (Piece piece : pieces) {
            ret += "    " + piece.toString() + "\n";
        }
        ret += "]\n";
        ret += "lanes:[\n";
        for (Lane lane : lanes) {
            ret += "    " + lane.toString() + "\n";
        }
        ret += "]\n";
        ret += "start: " + startingPoint.toString() + "\n";
        return ret;
    }
}

class Piece {
    protected double length;
    protected double radius;
    protected double angle;
    @SerializedName("switch")
    protected boolean isSwitch;

    Piece(double length, double radius, double angle, boolean isSwitch) {
        this.length = length;
        this.radius = radius;
        this.angle = angle;
        this.isSwitch = isSwitch;
    }

    @Override
    public String toString() {
        return "{ length = " + length + ", radius = " + radius + ", angle = " + angle + ", switch = " + isSwitch + "}";
    }
}

class Lane implements Comparable<Lane> {
    protected double distanceFromCenter;
    protected int index;

    Lane(double distanceFromCenter, int index) {
        this.distanceFromCenter = distanceFromCenter;
        this.index = index;
    }

    @Override
    public String toString() {
        return "{ distance = " + distanceFromCenter + ", index = " + index + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        Lane lane = (Lane)obj;
        return index == lane.index;
    }

    @Override
    public int compareTo(Lane lane) {
        return index - lane.index;
    }
}

class RaceSession {
    protected int laps;
    protected int maxLapTimeMs;
    protected boolean quickRace;

    RaceSession(int laps, int maxLapTimeMs, boolean quickRace) {
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.quickRace = quickRace;
    }

    @Override
    public String toString() {
        return "{laps = " + laps + ", maxLapTimeMs = " + maxLapTimeMs + ", quickRace = " + quickRace + "}";
    }
}

class StartingPoint {
    protected Position position;
    protected double angle;

    StartingPoint(Position position, double angle) {
        this.position = position;
        this.angle = angle;
    }

    @Override
    public String toString() {
        return "{" + position.toString() + ", " + angle + "}";
    }
}

class Position {
    protected double x;
    protected double y;

    Position(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "{" + x + ", " + y + "}";
    }
}

class GameInitData {
    Race race;

    GameInitData(Race race) {
        this.race = race;
    }
}

public class GameInitWrapper {
    protected String msgType;
    protected GameInitData data;

    public GameInitWrapper(String msgType, GameInitData data) {
        this.msgType = msgType;
        this.data = data;
    }
}
