package bot808;

import java.util.ArrayList;

public class OptimalPathFinder {
    final double EPS = 1e-9;
    Logger logger;
    ArrayList<Piece> pieces;
    ArrayList<Lane> lanes;
    double[][][] bestLength;
    double[][][] distCache;
    int[][][] next;
    int laps;
    double bestAnswer;
    int[] best;
    int[] cur;

    public OptimalPathFinder(GameInitWrapper gameInitWrapper, Logger logger) {
        this.logger = logger;
        this.pieces = (ArrayList<Piece>) gameInitWrapper.data.race.track.pieces.clone();
        this.lanes = (ArrayList<Lane>) gameInitWrapper.data.race.track.lanes.clone();
        bestLength = new double[pieces.size() + 1][lanes.size()][lanes.size()];
        next = new int[pieces.size() + 1][lanes.size()][lanes.size()];
        this.laps = gameInitWrapper.data.race.raceSession.laps;
        distCache = new double[pieces.size()][lanes.size()][lanes.size()];
        calcIt();
    }

    private void calcIt() {
        calcAllDist();
        int n = pieces.size();
        int m = lanes.size();
        for (int needLane = 0; needLane < m; needLane++) {
            for (int ourLane = 0; ourLane < m; ourLane++) {
                bestLength[n][ourLane][needLane] = Double.POSITIVE_INFINITY;
                next[n][ourLane][needLane] = -2;
            }
            bestLength[n][needLane][needLane] = 0;
        }
        for (int pieceId = n - 1; pieceId >= 0; pieceId--) {
            for (int ourLaneId = 0; ourLaneId < m; ourLaneId++) {
                for (int needLaneId = 0; needLaneId < m; needLaneId++) {
                    Piece piece = pieces.get(pieceId);
                    Lane lane = lanes.get(ourLaneId);
                    double curBest = bestLength[pieceId + 1][ourLaneId][needLaneId]
                            + distCache[pieceId][ourLaneId][ourLaneId];
                    int curNext = 0;
                    if (piece.isSwitch) {
                        if (ourLaneId > 0) {
                            double val = bestLength[pieceId + 1][ourLaneId - 1][needLaneId]
                                    + distCache[pieceId][ourLaneId][ourLaneId - 1];
                            if (compare(val, curBest) < 0) {
                                curBest = val;
                                curNext = -1;
                            }
                        }
                        if (ourLaneId + 1 < m) {
                            double val = bestLength[pieceId + 1][ourLaneId + 1][needLaneId]
                                    + distCache[pieceId][ourLaneId][ourLaneId + 1];
                            if (compare(val, curBest) < 0) {
                                curBest = val;
                                curNext = 1;
                            }
                        }
                    }
                    bestLength[pieceId][ourLaneId][needLaneId] = curBest;
                    next[pieceId][ourLaneId][needLaneId] = curNext;
                }
            }
        }
    }

    private void calcAllDist() {
        for (int pieceId = 0; pieceId < pieces.size(); pieceId++) {
            Piece piece = pieces.get(pieceId);
            for (int ourLaneId = 0; ourLaneId < lanes.size(); ourLaneId++) {
                Lane lane = lanes.get(ourLaneId);
                distCache[pieceId][ourLaneId][ourLaneId] = calcDist(piece, lane);
                if (ourLaneId > 0) {
                    distCache[pieceId][ourLaneId][ourLaneId - 1] = calcSwitchDist(piece, lane, lanes.get(ourLaneId - 1));
                }
                if (ourLaneId + 1 < lanes.size()) {
                    distCache[pieceId][ourLaneId][ourLaneId + 1] = calcSwitchDist(piece, lane, lanes.get(ourLaneId + 1));
                }
            }
        }
    }

    public void dump() {
        logger.print("Dumping bestLength");
        {
            String ret = "\n";
            for (int pieceId = 0; pieceId <= pieces.size(); pieceId++) {
                for (int ourLane = 0; ourLane < lanes.size(); ourLane++) {
                    for (int needLane = 0; needLane < lanes.size(); needLane++) {
                        ret += bestLength[pieceId][ourLane][needLane] + " ";
                    }
                    ret += "\n";
                }
                ret += "====================\n";
            }
            logger.print(ret);
        }
        logger.print("bestLength dumped");
        logger.print("Dumping next");

        {
            String ret = "\n";
            for (int pieceId = 0; pieceId <= pieces.size(); pieceId++) {
                for (int ourLane = 0; ourLane < lanes.size(); ourLane++) {
                    for (int needLane = 0; needLane < lanes.size(); needLane++) {
                        ret += next[pieceId][ourLane][needLane] + " ";
                    }
                    ret += "\n";
                }
                ret += "====================\n";
            }
            logger.print(ret);
        }
        logger.print("next dumped");
    }

    private void rec(int curLap, int curLane, double curAnswer) {
        if (curLap == laps) {
            if (compare(curAnswer, bestAnswer) < 0) {
                bestAnswer = curAnswer;
                best = cur.clone();
            }
            return;
        }
        for (int newLane = 0; newLane < lanes.size(); newLane++) {
            cur[curLap] = newLane;
            rec(curLap + 1, newLane, curAnswer + bestLength[0][curLane][newLane]);
        }
    }

    public int[] calcOptimalEnds(int startLane) {
        bestAnswer = Double.POSITIVE_INFINITY;
        best = new int[laps];
        cur = new int[laps];
        rec(0, startLane, 0.0);
        return best;
    }

    public int getNext(int pieceId, int ourLaneId, int needLaneId) {
        if (pieceId == pieces.size()) {
            return -2;
        }
        return next[pieceId][ourLaneId][needLaneId];
    }

    private double calcSwitchDist(Piece piece, Lane lane, Lane nextLane) {
        if (compare(piece.length, 0.0) == 0) {
            RoundCurve roundCurve = new RoundCurve(piece.radius - Math.signum(piece.angle) * lane.distanceFromCenter,
                    piece.radius - Math.signum(piece.angle) * nextLane.distanceFromCenter,
                    Math.abs(piece.angle) * Math.PI / 180.0);
            //double ret = Integrator.integrateSimpson(roundCurve, 0.0, 1.0);
            double ret = roundCurve.g(Math.abs(piece.angle) * Math.PI / 180.0) - roundCurve.g(0);
            logger.print("Distance for " + piece.toString() + " " + lane.toString()
                    + " " + nextLane.toString() + " = " + ret);
            return ret;
        } else {
            StraightCurve straightCurve = new StraightCurve(piece.length,
                    Math.abs(lane.distanceFromCenter - nextLane.distanceFromCenter));
            double ret = Integrator.integrateSimpson(straightCurve, 0.0, 1.0);
            logger.print("Distance for " + piece.toString() + " " + lane.toString()
                    + " " + nextLane.toString() + " = " + ret);
            return ret;
        }
    }

    private double calcDist(Piece piece, Lane lane) {
        if (compare(piece.length, 0.0) == 0) {
            double angle = Math.PI * piece.angle / 180.0;
            return Math.abs(angle) * piece.radius - lane.distanceFromCenter * angle;
        } else {
            return piece.length;
        }
    }

    private int compare(double a, double b) {
        if (Math.abs(a - b) < EPS) {
            return 0;
        }
        return a < b ? -1 : 1;
    }

}
