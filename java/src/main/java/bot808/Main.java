package bot808;

import com.google.gson.Gson;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    static Logger logger;
    static Logger mapLogger;
    static Logger gameLogger;
    static Logger pathFinderLogger;
    final Gson gson = new Gson();
    private PrintWriter writer;

    // TODO : make it OK
    double lastAngle;
    int lastPiece;
    double lastPositionInPiece;
    double friction;
    double mass;
    double zeroCoordinate;
    double firstCoordinate;
    double secondCoordinate;
    double lastThrottle;
    double totalDistance;
    double speed;
    PieceLane lastLane;
    Piece pieces[];
    double sumDists[];

    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey,
                final String raceType, final String password, final String trackName, final int carCount) throws IOException, CloneNotSupportedException {
        this.writer = writer;
        String line = null;
        lastPiece = -1;
        lastThrottle = -1;
        totalDistance = 0;
        if (raceType.equals("join")) {
            send(new Join(botName, botKey));
        } else if (raceType.equals("createRace")) {
            if (password == null || trackName == null) {
                logger.print("password or trackName is null");
                throw new AssertionError();
            }
            send(new CreateRace(new BotId(botName, botKey), trackName, password, carCount));
        } else if (raceType.equals("joinRace")) {
            if (password == null || trackName == null) {
                logger.print("password or trackName is null");
                throw new AssertionError();
            }
            send(new JoinRace(new BotId(botName, botKey), trackName, password, carCount));
        } else {
            logger.print("Can't understand what " + raceType + " is");
            throw new AssertionError();
        }

        CarId ourCar = null;
        int lastLap = -1;
        OptimalPathFinder pathFinder = null;
        boolean optimalEndsCalculated = false;
        int[] optimalEnds = null;
        boolean[] sentSwitch = null;
        int[] needSwitch = null;
        int lastSwitch = 0;
        while ((line = reader.readLine()) != null) {
            logger.print("Received from server: " + line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                logger.print("Received carPositions from server");
                final CarPositionsWrapper carPositionsWrapper = gson.fromJson(line, CarPositionsWrapper.class);
                CarData ourData = findCar(carPositionsWrapper.data, ourCar);
                if (ourData == null) {
                    // TODO: more informative log here
                    logger.print("Can't find our car");
                    throw new AssertionError();
                }
                if (!optimalEndsCalculated) {
                    logger.print("Calculating optimal ends");
                    if (pathFinder == null) {
                        logger.print("Variable pathFinder is null");
                        throw new AssertionError();
                    }
                    optimalEnds = pathFinder.calcOptimalEnds(ourData.piecePosition.lane.startLaneIndex);
                    if (optimalEnds == null) {
                        logger.print("Variable optimalEnds is null");
                        throw new AssertionError();
                    }
                    optimalEndsCalculated = true;
                    logger.print("Optimal ends calculated: " + Arrays.toString(optimalEnds));
                }
                logger.print("Dumping tick " + carPositionsWrapper.gameTick);
                gameLogger.print(carPositionsWrapper.gameTick + " " + ourData.piecePosition.pieceIndex
                        + " " + ourData.piecePosition.inPieceDistance + " " + ourData.piecePosition.lane.startLaneIndex
                        + " " + ourData.piecePosition.lane.endLaneIndex + " " + ourData.angle);
                logger.print("Tick " + carPositionsWrapper.gameTick + " dumped");
                if (lastLap != ourData.piecePosition.lap && ourData.piecePosition.lap < optimalEnds.length) {
                    logger.print("New lap have started. Recalculating needed data");
                    recalc(ourData.piecePosition.lane.endLaneIndex, needSwitch, sentSwitch,
                            optimalEnds[ourData.piecePosition.lap], pathFinder);
                    logger.print("needSwitch : " + Arrays.toString(needSwitch));
                    lastSwitch = 0;
                    lastLap = ourData.piecePosition.lap;
                }
                if (carPositionsWrapper.gameTick == 0) {
                    zeroCoordinate = ourData.piecePosition.inPieceDistance;
                } else if (carPositionsWrapper.gameTick == 1) {
                    firstCoordinate = ourData.piecePosition.inPieceDistance;
                } else if (carPositionsWrapper.gameTick == 2) {
                    secondCoordinate = ourData.piecePosition.inPieceDistance;
                    calcPhysicsConstants();
                }
                boolean messageSent = false;
                while (lastSwitch <= ourData.piecePosition.pieceIndex) {
                    lastSwitch++;
                }
                if (lastSwitch == ourData.piecePosition.pieceIndex + 1 && lastSwitch < needSwitch.length
                        && needSwitch[lastSwitch] != 0 && !sentSwitch[lastSwitch]) {
                    send(new SwitchLane(getDirection(needSwitch[lastSwitch])));
                    sentSwitch[lastSwitch] = true;
                    messageSent = true;
                }
                if (lastPiece != ourData.piecePosition.pieceIndex && lastPiece != -1) {
                    totalDistance += pathFinder.distCache[lastPiece][lastLane.startLaneIndex][lastLane.endLaneIndex];
                }
                lastLane = ourData.piecePosition.lane;
                lastPiece = ourData.piecePosition.pieceIndex;
                lastAngle = ourData.angle;
                speed = totalDistance + ourData.piecePosition.inPieceDistance - lastPositionInPiece;
                lastPositionInPiece = totalDistance + ourData.piecePosition.inPieceDistance;
                if (!messageSent) {
                    double throttle = getThrottle(ourData, carPositionsWrapper.gameTick, pathFinder);
                    send(new Throttle(throttle));
                    lastThrottle = throttle;
                }
            } else if (msgFromServer.msgType.equals("join")) {
                logger.print("Received join from server");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                logger.print("Received gameInit from server");
                final GameInitWrapper gameInitWrapper = gson.fromJson(line, GameInitWrapper.class);
                logger.print("Dumping map");
                mapLogger.print(gameInitWrapper.data.race.toString());
                logger.print("Map dumped");
                sentSwitch = new boolean[gameInitWrapper.data.race.track.pieces.size()];
                needSwitch = new int[gameInitWrapper.data.race.track.pieces.size()];
                pieces = gameInitWrapper.data.race.track.pieces.toArray(new Piece[]{});
                logger.print("Calculating optimal paths");
                pathFinder = new OptimalPathFinder(gameInitWrapper, pathFinderLogger);
                pathFinder.dump();
                calcSumDists(pieces.length, pathFinder);
                pathFinderLogger.close();
                logger.print("Optimal paths calculated");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                logger.print("Received gameEnd from server");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                logger.print("Received gameStart from server");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                logger.print("Received yourCar from server");
                final YourCarWrapper yourCarWrapper = gson.fromJson(line, YourCarWrapper.class);
                ourCar = yourCarWrapper.data.clone();
            } else {
                send(new Ping());
            }
        }

        logger.close();
        mapLogger.close();
        gameLogger.close();
        pathFinderLogger.close();
    }

    private void calcSumDists(int length, OptimalPathFinder pathFinder) {
        sumDists = new double[length];
        for (int i = 0; i < length; i++) {
            sumDists[i] = pathFinder.distCache[i][0][0];
            if (i > 0) {
                sumDists[i] += sumDists[i - 1];
            }
        }
    }

    final double BAD_ANGLE = 5;
    final double SPEED_IN_TURN = 6.2;
    private double getThrottle(CarData ourData, int gameTick, OptimalPathFinder pathFinder) {
        // HERE IS SOME FUCKING MAGIC
        if (gameTick < 3) {
            return 1.0;
        }
        int nextTurn = findNextTurn(ourData.piecePosition.pieceIndex);
        double distToTurn = pathFinder.distCache[ourData.piecePosition.pieceIndex]
                [ourData.piecePosition.lane.startLaneIndex][ourData.piecePosition.lane.endLaneIndex]
                - ourData.piecePosition.inPieceDistance;
        if (nextTurn == ourData.piecePosition.pieceIndex) {
            return SPEED_IN_TURN * friction;
        }
        distToTurn += calcDist(ourData.piecePosition.pieceIndex, nextTurn - 1);
        int neededTicks = 1;
        boolean good = true;
        double curSpeed = speed;// * z + 1 / friction;
//        distToTurn -= curSpeed;
        double x = 0.0;
        while (true) {
            x += curSpeed;
            curSpeed *=  1 - friction / mass;
            if (curSpeed < SPEED_IN_TURN) {
                good = true;
                break;
            }
            if (x >= distToTurn) {
                good = false;
                break;
            }
        }
        logger.print("gameTick = " + gameTick + " distToTurn = " + distToTurn + " good = " + good);
        return good ? 1.0 : 0.0;
    }

    private double calcDist(int from, int to) {
        // sum on (from, to]
        if (from <= to) {
            return sumDists[to] - sumDists[from];
        } else {
            return sumDists[to] + sumDists[sumDists.length - 1] - sumDists[from];
        }
    }

    private int findNextTurn(int pieceIndex) {
        for (int i = 0; i < pieces.length; i++) {
            int cur = (pieceIndex + i) % pieces.length;
            if (pieces[cur].radius > 0) {
                return cur;
            }
        }
        return pieceIndex;
    }

    private void calcPhysicsConstants() {
        double x1 = firstCoordinate - zeroCoordinate;
        double x2 = secondCoordinate - zeroCoordinate;
        friction = (3 * x1 - x2) / (x1 * x1);
        mass = 1 / x1;
    }

    public static void main(String... args) throws IOException, CloneNotSupportedException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String raceType = "join";
        String password = null;
        String trackName = null;
        int carCount = 0;
        if (args.length > 4) {
            raceType = args[4];
            password = args[5];
            trackName = args[6];
            carCount = Integer.parseInt(args[7]);
        }
        logger = new Logger(botName, "info.log");
        mapLogger = new Logger(botName, "map.log");
        gameLogger = new Logger(botName, "game.log");
        pathFinderLogger = new Logger(botName, "pathFinder.log");
        logger.print("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey, raceType, password, trackName, carCount);
    }

    private void recalc(int startLane, int[] needSwitch, boolean[] sentSwitch, int needLane, OptimalPathFinder pathFinder) {
        Arrays.fill(sentSwitch, false);
        int pieceId = 0;
        int curLane = startLane;
        while (true) {
            int next = pathFinder.getNext(pieceId, curLane, needLane);
            if (next == -2) {
                break;
            }
            needSwitch[pieceId] = next;
            curLane += next;
            pieceId++;
        }
    }

    private String getDirection(int dir) {
        logger.print(dir + "");
        return dir == -1 ? "Left" : "Right";
    }

    private CarData findCar(ArrayList<CarData> data, CarId ourCar) {
        for (CarData carData : data) {
            if (carData.id.equals(ourCar)) {
                return carData;
            }
        }
        return null;
    }


    private void send(final SendMsg msg) {
        logger.print("Sending: " + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
        logger.print("Message sent");
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class BotId {
    protected String name;
    protected String key;

    BotId(String name, String key) {
        this.name = name;
        this.key = key;
    }
}

class SwitchLane extends SendMsg {
    protected String direction;

    SwitchLane(String direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class JoinRace extends SendMsg {
    protected BotId botId;
    protected String trackName;
    protected String password;
    protected int carCount;

    JoinRace(BotId botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class CreateRace extends SendMsg {
    protected BotId botId;
    protected String trackName;
    protected String password;
    protected int carCount;

    CreateRace(BotId botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}