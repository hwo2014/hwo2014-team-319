package bot740;

import com.google.gson.Gson;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    final static int MAX_LAPS = 123;

    static Logger logger;
    static Logger mapLogger;
    static Logger gameLogger;
    static Logger pathFinderLogger;
    final Gson gson = new Gson();
    private PrintWriter writer;

    // TODO : make it OK
    boolean hasStarted;
    double lastPosition;
    double lastAngle;
    double friction;
    double mass;
    double zeroCoordinate;
    double firstCoordinate;
    double secondCoordinate;
    double lastThrottle;
    double totalDistance;
    double speed; // current speed
    double predictedSpeed; // predicted speed
    double predictedDa; // predicted angle speed
    double da; // current angle speed
    double carLength;
    double aF; // angle speed equaton parameters
    double aA;
    double aK;
    double aL;

    boolean physicsKnown;
    boolean anglePhysicsKnown;

    int laps;
    Piece[] pieces;
    Lane[] lanes;
    int currentPtr;
    ArrayList<Integer> optimalPathPieces;
    ArrayList<Integer> optimalPathLanes;
    boolean[] switchSent;
    double[] prefixSums;
    double[] maxSpeed;
    OptimalPathFinder pathFinder;

    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey,
                final String raceType, final String password, final String trackName, final int carCount) throws IOException, CloneNotSupportedException {
        aF = 0.9;
        aA = -0.00125;
        aK = -0.3;
        aL = 0.530330086;
        //anglePhysicsKnown = false;
        anglePhysicsKnown = true;
        this.writer = writer;
        String line = null;
        hasStarted = false;
        physicsKnown = false;
        if (raceType.equals("join")) {
            send(new Join(botName, botKey));
        } else if (raceType.equals("createRace")) {
            if (password == null || trackName == null) {
                logger.print("password or trackName is null");
                throw new AssertionError();
            }
            send(new CreateRace(new BotId(botName, botKey), trackName, password, carCount));
        } else if (raceType.equals("joinRace")) {
            if (password == null || trackName == null) {
                logger.print("password or trackName is null");
                throw new AssertionError();
            }
            send(new JoinRace(new BotId(botName, botKey), trackName, password, carCount));
        } else {
            logger.print("Can't understand what " + raceType + " is");
            throw new AssertionError();
        }

        CarId ourCar = null;
        int lastLap = -1;
        pathFinder = null;
        boolean optimalEndsCalculated = false;
        ArrayList<Double> positions = new ArrayList<Double>();
        ArrayList<Double> throttles = new ArrayList<Double>();
        boolean crashed = false;
        while ((line = reader.readLine()) != null) {
            if (crashed) {
                logger.print("!!!!!!WE HAVE CRASHED!!!!!!");
                predictedSpeed = predictedDa = lastAngle = 0;
                return;
            }
            logger.print("Received from server: " + line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                logger.print("Received carPositions from server");
                final CarPositionsWrapper carPositionsWrapper = gson.fromJson(line, CarPositionsWrapper.class);
                CarData ourData = findCar(carPositionsWrapper.data, ourCar);
                if (ourData == null) {
                    // TODO: more informative log here
                    logger.print("Can't find our car");
                    throw new AssertionError();
                }
                if (!optimalEndsCalculated) {
                    logger.print("Calculating optimal ends");
                    if (pathFinder == null) {
                        logger.print("Variable pathFinder is null");
                        throw new AssertionError();
                    }
                    int[] optimalEnds = pathFinder.calcOptimalEnds(ourData.piecePosition.lane.startLaneIndex);
                    if (optimalEnds == null) {
                        logger.print("Variable optimalEnds is null");
                        throw new AssertionError();
                    }
                    optimalEndsCalculated = true;
                    logger.print("Optimal ends calculated: " + Arrays.toString(optimalEnds));
                    logger.print("Getting optimal path");
                    getOptimalPath(pathFinder, optimalEnds, ourData.piecePosition.lane.startLaneIndex);
                    calcPrefixSums(pathFinder);
                    logger.print("Optimal path got");
                }
                logger.print("Dumping tick " + carPositionsWrapper.gameTick);
                gameLogger.print(carPositionsWrapper.gameTick + " " + ourData.piecePosition.pieceIndex
                        + " " + ourData.piecePosition.inPieceDistance + " " + ourData.piecePosition.lane.startLaneIndex
                        + " " + ourData.piecePosition.lane.endLaneIndex + "\t\ta, sp, bend = " + ourData.angle
                        + " " + predictedSpeed + " " + calcRadius(ourData));
                logger.print("Tick " + carPositionsWrapper.gameTick + " dumped");
                if (carPositionsWrapper.gameTick == 0) {
                    zeroCoordinate = ourData.piecePosition.inPieceDistance;
                } else if (carPositionsWrapper.gameTick == 1) {
                    firstCoordinate = ourData.piecePosition.inPieceDistance;
                } else if (carPositionsWrapper.gameTick >= 2 && !physicsKnown) {
                    secondCoordinate = ourData.piecePosition.inPieceDistance;
                    calcPhysicsConstants();
                }
                if (ourData.piecePosition.lap == 0 && ourData.piecePosition.pieceIndex == 0) {
                    if (!hasStarted) {
                        logger.print("Race has started!");
                        hasStarted = true;
                    }
                }
                int prevPiece = currentPtr;
                int switchDirection = getSwitchDirection(ourData.piecePosition.pieceIndex,
                        ourData.piecePosition.lap, carPositionsWrapper.gameTick);
                if (switchDirection != 0) {
                    send(new SwitchLane(getDirection(switchDirection)), carPositionsWrapper.gameTick);
                }
                update(ourData.piecePosition.pieceIndex);
                double curPosition = ourData.piecePosition.inPieceDistance + totalDistance;
                speed = curPosition - lastPosition;
                lastPosition = curPosition;

                positions.add(curPosition);
                da = ourData.angle - lastAngle;
                lastAngle = ourData.angle;

                logger.print("Tick " + carPositionsWrapper.gameTick + " speed = " + speed + " == " + predictedSpeed +
                        " da = " + da + " == " + predictedDa);
                if (OptimalPathFinder.compare(speed, predictedSpeed) != 0 && carPositionsWrapper.gameTick > 1) {
                    logger.print("!!!! Speed mismatch !!!!");
                    double delta = predictedSpeed - speed;
                    logger.print("prevPiece = " + prevPiece + " currentPtr = " + currentPtr);
                    if (prevPiece == currentPtr - 1) { //adjust length of last piece.
                        pathFinder.distCache[optimalPathPieces.get(currentPtr - 1)]
                                [optimalPathLanes.get(currentPtr - 1)]
                                [optimalPathLanes.get(currentPtr)] += delta;
                        speed = predictedSpeed;
                    } else {
                       logger.print("Speed prediction failed");
                        predictedSpeed = speed;
                    }
                }

                if (Math.abs(da - predictedDa) > 1e-5) {
                    logger.print("!!!! angle mismatch !!!!");
                    da = predictedDa;
                }
                if (switchDirection == 0 && physicsKnown) {
                    double throttle = getThrottle(ourData, carPositionsWrapper.gameTick);
                    send(new Throttle(throttle), carPositionsWrapper.gameTick);
                    lastThrottle = throttle;
                    throttles.add(throttle);
                } else if (switchDirection == 0) {
                    send(new Throttle(1.0), carPositionsWrapper.gameTick);
                    lastThrottle = 1.0;
                    throttles.add(1.0);
                }


                if (anglePhysicsKnown) {
                    double drift = 0;
                    if (pieces[ourData.piecePosition.pieceIndex].radius > 0) // drift force is there if not in stright road
                        drift = (aK + aL * predictedSpeed / Math.sqrt(calcRadius(ourData))) * predictedSpeed;
                    if (drift < 0) drift = 0; // can't be negative
                    if (pieces[ourData.piecePosition.pieceIndex].angle < 0) // drift force is applied in correct direction
                        drift = -drift;
                    predictedDa = aF * predictedDa + aA * ourData.angle * predictedSpeed + drift;
                }

                if (physicsKnown) {
                    predictedSpeed = lastThrottle / mass + predictedSpeed * (1 - friction / mass);
                }

            } else if (msgFromServer.msgType.equals("join")) {
                logger.print("Received join from server");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                logger.print("Received gameInit from server");
                final GameInitWrapper gameInitWrapper = gson.fromJson(line, GameInitWrapper.class);
                if (gameInitWrapper.data.race.raceSession.laps == 0) {
                    logger.print("We got zero laps!");
                    gameInitWrapper.data.race.raceSession.laps = MAX_LAPS;
                }
                laps = gameInitWrapper.data.race.raceSession.laps;
                logger.print("Dumping map");
                mapLogger.print(gameInitWrapper.data.race.toString());
                logger.print("Map dumped");
                pieces = gameInitWrapper.data.race.track.pieces.toArray(new Piece[]{});
                lanes = gameInitWrapper.data.race.track.lanes.toArray(new Lane[]{});
                carLength = gameInitWrapper.data.race.cars.get(0).dimensions.length;
                logger.print("Calculating optimal paths");
                pathFinder = new OptimalPathFinder(gameInitWrapper, pathFinderLogger);
                pathFinder.dump();
                pathFinderLogger.close();
                logger.print("Optimal paths calculated");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                logger.print("Received gameEnd from server");
                logger.print("positions: " + Arrays.toString(positions.toArray(new Double[]{})));
                logger.print("throttles: " + Arrays.toString(throttles.toArray(new Double[]{})));
            } else if (msgFromServer.msgType.equals("gameStart")) {
                logger.print("Received gameStart from server");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                logger.print("Received yourCar from server");
                final YourCarWrapper yourCarWrapper = gson.fromJson(line, YourCarWrapper.class);
                ourCar = yourCarWrapper.data.clone();
            } else if (msgFromServer.msgType.equals("crash")) {
                logger.print("!!!!!!!!!!WE HAVE CRASHED!!!!!!!!!");
                crashed = true;
            } else if (msgFromServer.msgType.equals("spawn")) {
                logger.print("!!!!! WE HAVE SPAWNED!!!!");
                crashed = false;
            } else {
                send(new Ping());
            }
        }

        logger.close();
        mapLogger.close();
        gameLogger.close();
        pathFinderLogger.close();
    }

    class State {
        public double x;
        public double v;
        public double a;
        public double da;
        int i;

        State(double x, double v, double a, double da, int i) {
            this.x = x;
            this.v = v;
            this.a = a;
            this.da = da;
            this.i = i;
        }

        State next(double tr)
        {
            State tmp = new State(x,v,a,da,i);
            tmp.nextInPlace(tr);
            return tmp;
        }

        void nextInPlace(double tr) {
            double R = calcRadius(this);
            double alpha = pieces[optimalPathPieces.get(i)].angle;
            double drift = 0;
            if (R > 0) drift = aK * v + aL*v*v/Math.sqrt(R);
            if (drift < 0) drift = 0;
            if (alpha < 0) drift = -drift;
            da = aF * da + aA * v * a + drift;
            a = a + da;
            v = (1 - friction / mass) * v + tr / mass;
            x = x + v;
            i = i;

            while (i < optimalPathPieces.size() &&
                    x > pathFinder.distCache[optimalPathPieces.get(i)]
                            [optimalPathLanes.get(i)]
                            [optimalPathLanes.get(i+1)]) {
                x -= pathFinder.distCache[optimalPathPieces.get(i)]
                        [optimalPathLanes.get(i)]
                        [optimalPathLanes.get(i+1)];
                i++;
            }
        }
    }



    public static final double BAD_ANGLE = 59.5;
    public static final int MAX_SIMULATE_ITERATIONS = 40;
    public static final int MAX_BINSEARCH_ITERATIONS = 15;
    public static final int MAX_PREDICT_ITERATIONS = 50;

    double calcCriticalSpeed(State cur) {
        double maxSpeed = 1000000.0;
        if (pieces[optimalPathPieces.get(cur.i)].radius > 0) {
            maxSpeed = -aK / aL * Math.sqrt(calcRadius(cur));
        }
        int n = optimalPathPieces.size();
        double curDist = calcLength(cur.i)-cur.x;
        for (int j = cur.i+1; j < n; j++) {
            if (pieces[optimalPathPieces.get(j)].radius > 0) {
                double R1 = calcRadius(optimalPathPieces.get(j), 0,
                        optimalPathLanes.get(j), optimalPathLanes.get(j));
                double R2 = calcRadius(optimalPathPieces.get(j), 0,
                        optimalPathLanes.get(j+1), optimalPathLanes.get(j+1));
                double curSpeed = -aK / aL * Math.sqrt(Math.max(R1,R2)); // dirty fix. may need mean, max or min here
                curSpeed += curDist * friction / mass;
                if (curSpeed < maxSpeed) maxSpeed = curSpeed;
            }
            curDist += calcLength(j);
        }
        return maxSpeed;
    }

    boolean Simulate(State init, double throttle0, double minSpeed) {
        State cur = init.next(throttle0);
        int it = 0;
        while (cur.i < optimalPathPieces.size()) {
            if (Math.abs(cur.a) >= BAD_ANGLE)
                return false;
            if (it > MAX_SIMULATE_ITERATIONS) break;
            double maxAllowedSpeed = calcCriticalSpeed(cur);
            double tr = calcThrottle(cur.v, maxAllowedSpeed);
            tr = Math.max(tr, calcThrottle(cur.v, minSpeed));
            cur.nextInPlace(tr);
            it++;
        }
        return true;
    }

    // id in optimal path
    private double calcLength(int i) {
        return pathFinder.distCache[optimalPathPieces.get(i)][optimalPathLanes.get(i)][optimalPathLanes.get(i+1)];
    }

    private double calcRadius(CarData ourData) {
        return calcRadius(ourData.piecePosition.pieceIndex, ourData.piecePosition.inPieceDistance,
                ourData.piecePosition.lane.startLaneIndex, ourData.piecePosition.lane.endLaneIndex);
    }

    private  double calcRadius(State st) {
        return calcRadius(optimalPathPieces.get(st.i), st.x, optimalPathLanes.get(st.i), optimalPathLanes.get(st.i + 1));
    }

    private double calcRadius(int piece, double dist, int l1, int l2) {
        double r1 = pieces[piece].radius - Math.signum(pieces[piece].angle)*lanes[l1].distanceFromCenter;
        if (l1 != l2) {
            double r2 = pieces[piece].radius - Math.signum(pieces[piece].angle)*lanes[l2].distanceFromCenter;
            r1 += (r2 - r1) * dist / pathFinder.distCache[piece][l1][l2];
        }
        return r1;
    }

    private int getSwitchDirection(int pieceIndex, int lap, int gameTick) {
        if (gameTick == 0) {
            return 0;
        }
        logger.print("getSwitchDirection pieceIndex = " + pieceIndex + " lap = " + lap);
        update(pieceIndex);
        logger.print("after update currentPtr = " + currentPtr);
        if (currentPtr + 2 < optimalPathPieces.size() &&
                optimalPathLanes.get(currentPtr + 1) != optimalPathLanes.get(currentPtr + 2)) {
            logger.print("lane1 = " + optimalPathLanes.get(currentPtr + 1) +
                    " lane2 = " + optimalPathLanes.get(currentPtr + 2) + " switchSent = " + switchSent[currentPtr + 1]);
            if (!switchSent[currentPtr + 1]) {
                switchSent[currentPtr + 1] = true;
                logger.print("returning " + (optimalPathLanes.get(currentPtr + 2) - optimalPathLanes.get(currentPtr + 1)));
                return optimalPathLanes.get(currentPtr + 2) - optimalPathLanes.get(currentPtr + 1);
            } else {
                logger.print("returning 0");
                return 0;
            }
        }
        return 0;
    }

    void update(int curPieceId) {
        if (!hasStarted) {
            return;
        }
        logger.print("update: pieceId = " + curPieceId + " ptr = " + currentPtr);
        while (currentPtr + 1 < optimalPathPieces.size() && optimalPathPieces.get(currentPtr) != curPieceId) {
            currentPtr++;
            if (currentPtr < optimalPathLanes.size())
                totalDistance += prefixSums[currentPtr] - prefixSums[currentPtr - 1];
        }
    }

    private void calcPrefixSums(OptimalPathFinder pathFinder) {
        logger.print("Calculating prefix sums");
        prefixSums = new double[optimalPathPieces.size()];
        prefixSums[0] = 0;
        for (int i = 0; i < prefixSums.length - 1; i++) {
            int curPieceId = optimalPathPieces.get(i);
            int curLaneId = optimalPathLanes.get(i);
            int nextLaneId = optimalPathLanes.get(i + 1);
            prefixSums[i + 1] = prefixSums[i] + pathFinder.distCache[curPieceId][curLaneId][nextLaneId];
        }
        logger.print("prefixSums : " + Arrays.toString(prefixSums));
    }

    private void getOptimalPath(OptimalPathFinder pathFinder, int[] optimalEnds, int startLaneId) {
        currentPtr = 0;
        optimalPathPieces = new ArrayList<Integer>();
        optimalPathLanes = new ArrayList<Integer>();
        int curLaneId = startLaneId;
        for (int lap = 0; lap < laps; lap++) {
            int pieceId = 0;
            while (true) {
                int next = pathFinder.getNext(pieceId, curLaneId, optimalEnds[lap]);
                if (next == -2) {
                    break;
                }
                optimalPathPieces.add(pieceId);
                optimalPathLanes.add(curLaneId);
                pieceId++;
                curLaneId += next;
            }
        }
        //optimalPathPieces.add(0);
        optimalPathLanes.add(optimalEnds[laps - 1]);
        switchSent = new boolean[optimalPathLanes.size()];
        Arrays.fill(switchSent, false);
        String optimalPath = "\n";
        for (int i = 0; i < optimalPathPieces.size(); i++) {
            optimalPath += "(" + optimalPathPieces.get(i) + ", " + optimalPathLanes.get(i) + ")\n";
        }
        logger.print("optimal path:" + optimalPath);
    }


    double BinSearchFirstThrottle(State init, double minSpeed)
    {
        double mn, mx;
        mn = calcThrottle(init.v, calcCriticalSpeed(init));
        mx = 1.0;
        for (int it=0; it < MAX_BINSEARCH_ITERATIONS; it++) {
            double mid = (mn+mx)/2.0;
            if (Simulate(init, mid,  minSpeed))
                mn = mid;
            else mx = mid;
        }
        return mn;
    }

    State PredictTravel(State init, double minSpeed)
    {
        double ans = 0.0;
        State cur = new State(init.x, init.v, init.a, init.da, init.i);
        for (int i = 0; i < MAX_PREDICT_ITERATIONS; i++) {
            double tr = BinSearchFirstThrottle(init, minSpeed);
            cur.nextInPlace(tr);
            if (cur.i > optimalPathPieces.size()) break;
        }
        return cur;
    }
    private double getThrottle(CarData ourData, int gameTick) {
        logger.print("getThrottle: pieceId = " + ourData.piecePosition.pieceIndex + " gameTick = " + gameTick);
        // HERE IS SOME FUCKING MAGIC
        if (gameTick < 3 || !hasStarted || !physicsKnown || !anglePhysicsKnown) {
            return 1.0;
        }
        State init = new State(ourData.piecePosition.inPieceDistance, predictedSpeed,
                ourData.angle, predictedDa, currentPtr);
        double minSpeed = 0;
        double L, R;
        L = 0; R = 1.0 / friction;
        if (init.v < calcCriticalSpeed(init))
            return BinSearchFirstThrottle(init, 0);; //do not calculate all the stuff, if we can accelerate now.
        for (int i = 0; i < MAX_BINSEARCH_ITERATIONS; i++) {
            double m1, m2;
            m1 = (L+L+R)/3.0;
            m2 = (L+R+R)/3.0;
            State s1 = PredictTravel(init, m1);
            State s2 = PredictTravel(init, m2);
            if (s1.i > s2.i || (s1.i == s2.i && s1.x > s2.x))
                R = m1;
            else
                L = m2;
        }
        logger.print("Speed threshold = ( " + L + " " + R +" ) ");
        return BinSearchFirstThrottle(init, R);
    }

    private double calcThrottle(double v0, double v1) {
        double ret = Math.min(1.0, mass * v1 - (mass - friction) * v0);
        if (ret < 0)
            ret = 0;
        return ret;
    }

    private void calcPhysicsConstants() {
        double x1 = firstCoordinate - zeroCoordinate;
        double x2 = secondCoordinate - zeroCoordinate;
        if (x1 > 0 && x2 > x1) {
            friction = (3 * x1 - x2) / (x1 * x1);
            mass = 1 / x1;
            double k = (1 - friction / mass);
            predictedSpeed = 0;
            for (int tick = 0; tick < 2; tick++)
                predictedSpeed = predictedSpeed * k + 1 / mass;
            assert (predictedSpeed == x2 - x1);
            physicsKnown = true;
            logger.print("physics: " + "friction = " + friction + " mass = " + mass);
        } else {
            zeroCoordinate = firstCoordinate;
            firstCoordinate = secondCoordinate;
        }
    }

    public static void main(String... args) throws IOException, CloneNotSupportedException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String raceType = "join";
        String password = null;
        String trackName = null;
        int carCount = 0;
        if (args.length > 4) {
            raceType = args[4];
            password = args[5];
            trackName = args[6];
            carCount = Integer.parseInt(args[7]);
        }
        logger = new Logger(botName, "info.log");
        mapLogger = new Logger(botName, "map.log");
        gameLogger = new Logger(botName, "game.log");
        pathFinderLogger = new Logger(botName, "pathFinder.log");
        logger.print("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey, raceType, password, trackName, carCount);
    }


    private String getDirection(int dir) {
        logger.print(dir + "");
        return dir == -1 ? "Left" : "Right";
    }

    private CarData findCar(ArrayList<CarData> data, CarId ourCar) {
        for (CarData carData : data) {
            if (carData.id.equals(ourCar)) {
                return carData;
            }
        }
        return null;
    }


    private void send(final SendMsg msg) {
        logger.print("Sending: " + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
        logger.print("Message sent");
    }

    private void send(final SendMsg msg, int tick) {
        String str = msg.toJson();
        str = str.substring(0, str.length()-1) + ", \"gameTick\": " + tick + "}";
        logger.print("Sending: " + str);
        writer.println(str);
        writer.flush();
        logger.print("Message sent");
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class BotId {
    protected String name;
    protected String key;

    BotId(String name, String key) {
        this.name = name;
        this.key = key;
    }
}

class SwitchLane extends SendMsg {
    protected String direction;

    SwitchLane(String direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class JoinRace extends SendMsg {
    protected BotId botId;
    protected String trackName;
    protected String password;
    protected int carCount;

    JoinRace(BotId botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class CreateRace extends SendMsg {
    protected BotId botId;
    protected String trackName;
    protected String password;
    protected int carCount;

    CreateRace(BotId botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}