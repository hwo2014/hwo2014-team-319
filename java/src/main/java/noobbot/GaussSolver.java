package noobbot;

import java.util.ArrayList;

class Vector {
    final static double EPS = 1e-9;
    double[] x;

    Vector(double[] x) {
        this.x = x;
    }

    Vector(int n) {
        this.x = new double[n];
    }
    Vector add(Vector o) {
        if (x.length != o.x.length) {
            throw new AssertionError("Wrong dimensions");
        }
        double[] nx = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            nx[i] = x[i] + o.x[i];
        }
        return new Vector(nx);
    }

    Vector subtract(Vector o) {
        if (x.length != o.x.length) {
            throw new AssertionError("Wrong dimensions");
        }
        double[] nx = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            nx[i] = x[i] - o.x[i];
        }
        return new Vector(nx);
    }

    Vector multiply(double k) {
        double[] nx = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            nx[i] = x[i] * k;
        }
        return new Vector(nx);
    }

    double scalar(Vector o) {
        if (x.length != o.x.length) {
            throw new AssertionError("Wrong dimensions");
        }
        double ret = 0;
        for (int i = 0; i < x.length; i++) {
            ret += x[i] * o.x[i];
        }
        return ret;
    }

    boolean isZero() {
        for (double xs: x) {
            if (Math.abs(xs) > EPS) {
                return false;
            }
        }
        return true;
    }

    Vector copy() {
        return new Vector(x);
    }

    public void inPlaceMultiply(double k) {
        for (int i = 0; i < x.length; i++) {
            x[i] *= k;
        }
    }

    public double len() {
        double res = 0;
        for (int i = 0; i < x.length; i++) {
            res += x[i]*x[i];
        }
        return Math.sqrt(res);
    }
}


// Need to rewrite this shit
class GaussSolver {
    final static double EPS = 1e-9;
    ArrayList<Vector> vectors;
    ArrayList<Double> bs;
    ArrayList<Integer> pivots;

    Vector tmp;

    GaussSolver() {
        vectors = new ArrayList<Vector>();
        pivots = new ArrayList<Integer>();
        bs = new ArrayList<Double>();
    }

    /*
        returns:
            -1 -- if new equation is incorrect for system
            0 -- if new equation is linear dependent
            1 -- if new equation is linear independent, and adds it to system equations
     */
    int addEquationEnd(double b) {
        if (tmp == null) return 0;
        for (int i = 0; i < vectors.size(); i++) {
            Vector vector = vectors.get(i);
            int pivot = pivots.get(i);
            double t = vector.scalar(tmp);
            tmp = tmp.subtract(vector.multiply(t));
            b -= bs.get(i) * t;
        }
        if (tmp.isZero()) {
            tmp = null;
            return Math.abs(b) > EPS ? -1 : 0;
        }
        int pivot = -1;
        for( int i = 0; i < tmp.x.length; i++) {
            System.out.print(tmp.x[i] + " ");
        }
        System.out.println(" | " + b);
        double k = tmp.len();
        tmp.inPlaceMultiply(1 / k);

        vectors.add(tmp);
        pivots.add(pivot);
        bs.add(b / k);
        tmp = null;
        return 1;
    }

    void addEquation(double k1, double k2, double k3, double k4)
    {
        Vector tmp = new Vector(4);
        tmp.x[0] = k1;
        tmp.x[1] = k2;
        tmp.x[2] = k3;
        tmp.x[3] = k4;
        addEquationBeg(tmp);
    }

    void addEquationBeg(Vector o) {
        tmp = o.copy();
    }

    int size() {
        return vectors.size();
    }

    double[] solve() {
        int n = vectors.size();
        double[][] a = new double[n][n + 1];
        for (int i = 0; i < n; i++) {
            Vector vector = vectors.get(i);
            for (int j = 0; j < n; j++) {
                a[i][j] = vector.x[j];
                System.out.print(a[i][j] + " ");
            }
            System.out.println(" | " + bs.get(i));
            a[i][n] = bs.get(i);
        }
        for (int i = 0; i < n; i++) {
            int jj = 0;
            int ii;
            for (ii = 1; ii < n; ii++)
                if (Math.abs(a[i][jj]) < Math.abs(a[i][ii]))
                    jj = ii;
            pivots.set(i, jj);
            double t = a[i][jj];
            for (ii = 0; ii < n + 1; ii++)
                a[i][ii] /= t;
            for (ii = 0; ii < n; ii++) {
                if (ii == i) continue;
                double pivot = a[ii][jj];
                for (int j = 0; j <= n; j++) {
                    a[ii][j] -= a[i][j] * pivot;
                }
            }
        }
        double[] ret = new double[a.length];
        for (int i = 0; i < a.length;  i++) {
            int j = pivots.get(i);
            ret[j] = a[i][n];
        }
        // checking that solution is ok
        for (int i = 0; i < n; i++) {
            Vector vector = vectors.get(i);
            double sum = 0;
            for (int j = 0; j < n; j++) {
                sum += vector.x[j] * ret[j];
            }
            if (Math.abs(sum - bs.get(i)) > 1e-9) {
                throw new AssertionError();
            }
        }
        return ret;
    }
}
