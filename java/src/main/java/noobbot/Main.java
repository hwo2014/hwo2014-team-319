package noobbot;

import com.google.gson.Gson;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static final double BAD_ANGLE = 59.5;
    public static final int MAX_SIMULATE_ITERATIONS = 250;
    public static final int MAX_BINSEARCH_ITERATIONS = 12;
    public static final int MAX_PREDICT_ITERATIONS = 50;
    final static String secretTurboMessage = "Keep on rollin baby";
    final static int MAX_LAPS = 10;
    static Logger logger;
    static Logger mapLogger;
    static Logger gameLogger;
    static Logger pathFinderLogger;
    final Gson gson = new Gson();
    // TODO : make it OK
    double lastPosition;
    double lastAngle;
    double friction;
    double mass;
    double zeroCoordinate;
    double firstCoordinate;
    double secondCoordinate;
    double lastThrottle;
    double totalDistance;
    double speed; // current speed
    double predictedSpeed; // predicted speed
    double predictedDa; // predicted angle speed
    double da; // current angle speed
    double carLength;
    double aF; // angle speed equaton parameters
    double aA;
    double aK;
    double aL;
    boolean physicsKnown;
    boolean anglePhysicsKnown;
    int laps;
    Piece[] pieces;
    Lane[] lanes;
    boolean optimalPathCalculated;
    OptimalPathFinder pathFinder;
    int currentPtr;
    ArrayList<Integer> optimalPathPieces;
    ArrayList<Integer> optimalPathLanes;
    boolean[] switchSent;
    GaussSolver solver;
    //double[] prefixSums;
    double[] maxSpeed;
    ArrayList<Double> positions;
    ArrayList<Double> throttles;
    boolean crashed;
    boolean isQualification;
    boolean turboAvailable;
    TurboData turboData;
    int turboStart;
    int turboEnd;
    boolean angleMismatch;
    String mapName;
    Socket socket;
    boolean tickWasted;
    double suggestR3, suggestR2, suggestR1;
    CarId ourCar;
    private PrintWriter writer;

    public Main(final BufferedReader reader, final Socket socket, final PrintWriter writer, final String botName, final String botKey,
                final String raceType, final String password, final String trackName, final int carCount) throws IOException, CloneNotSupportedException {
        initAngleConstants();
        anglePhysicsKnown = false;
        physicsKnown = false;
        angleMismatch = false;
        this.writer = writer;
        this.socket = socket;
        String line = null;
        solver = new GaussSolver();
        turboData = new TurboData(0, 0, 1.0);
        if (raceType.equals("join")) {
            send(new Join(botName, botKey));
        } else if (raceType.equals("createRace")) {
            if (password == null || trackName == null) {
                logger.print("password or trackName is null");
                throw new AssertionError();
            }
            send(new CreateRace(new BotId(botName, botKey), trackName, password, carCount));
        } else if (raceType.equals("joinRace")) {
            if (password == null || trackName == null) {
                logger.print("password or trackName is null");
                throw new AssertionError();
            }
            send(new JoinRace(new BotId(botName, botKey), trackName, password, carCount));
        } else {
            logger.print("Can't understand what " + raceType + " is");
            throw new AssertionError();
        }

        while ((line = reader.readLine()) != null) {
            if (crashed) {
                logger.print("!!!!!!WE HAVE CRASHED!!!!!!");
                predictedSpeed = 0;
                predictedDa = 0;
                lastAngle = 0;
            }
            logger.print("Received from server: " + line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

            if (msgFromServer.msgType.equals("carPositions")) {
                logger.print("Received carPositions from server");
                final CarPositionsWrapper carPositionsWrapper = gson.fromJson(line, CarPositionsWrapper.class);
                CarData ourData = findCar(carPositionsWrapper.data, ourCar);
                if (ourData == null) {
                    // TODO: more informative log here
                    logger.print("Can't find our car");
                    throw new AssertionError();
                }
                if (!optimalPathCalculated) {
                    logger.print("Calculating optimal ends");
                    calculateOptimalPath(ourData.piecePosition);
                    logger.print("Optimal path got");
                }

                logger.print("Dumping tick " + carPositionsWrapper.gameTick);
                gameLogger.print(carPositionsWrapper.gameTick + " " + ourData.piecePosition.pieceIndex
                        + " " + ourData.piecePosition.inPieceDistance + " " + ourData.piecePosition.lane.startLaneIndex
                        + " " + ourData.piecePosition.lane.endLaneIndex + "\t\ta, sp, bend = " + ourData.angle
                        + " " + predictedSpeed + " " + calcRadius(ourData));
                logger.print("Tick " + carPositionsWrapper.gameTick + " dumped");

                int prevPiece = currentPtr;

                update(ourData.piecePosition.pieceIndex);

                double curPosition = ourData.piecePosition.inPieceDistance + totalDistance;

                positions.add(curPosition);
                if (positions.size() >= 3 && !physicsKnown) {
                    calcPhysicsConstants();
                }

                int switchDirection = getSwitchDirection(ourData.piecePosition.pieceIndex,
                        ourData.piecePosition.lap, carPositionsWrapper.gameTick);
                boolean messageSent = false;

                double throttle;
                throttle = getThrottle(ourData, carPositionsWrapper.gameTick);


                speed = curPosition - lastPosition;
                lastPosition = curPosition;

                da = ourData.angle - lastAngle;
                lastAngle = ourData.angle;

                logger.print("Tick " + carPositionsWrapper.gameTick + " speed_infered = " + speed + " speed_predicted = " + predictedSpeed +
                        " da = " + da + "  predicted_da = " + predictedDa + " isTurbo " + isTurboIteration(carPositionsWrapper.gameTick));

                if (OptimalPathFinder.compare(speed, predictedSpeed) != 0 && physicsKnown) {
                    logger.print("!!!! Speed mismatch !!!!");
                    fixSpeed(prevPiece, carPositionsWrapper.gameTick);
                }

                if (anglePhysicsKnown && Math.abs(da - predictedDa) > 1e-5) {
                    logger.print("!!!! angle mismatch !!!!");
                    double R = suggestR1 / (da - suggestR2);
                    R = R * R;
                    angleMismatch = true;
                    logger.print("suggested radius:  " + R);
                    logger.print("calculated radius: " + suggestR3);
                    predictedDa = da;
                } else angleMismatch = false;

                if (switchDirection != 0) {
                    if (throttle >= lastThrottle) {
                        messageSent = true;
                        switchSent[currentPtr + 1] = true;
                        sendWithTick(new TickSwitchLane(getDirection(switchDirection), carPositionsWrapper.gameTick));
                    }
//                    send(new SwitchLane(getDirection(switchDirection)), carPositionsWrapper.gameTick);
                }

                if (turboAvailable && !messageSent && IsGoodPieceForTurbo(ourData.piecePosition.pieceIndex)) {
                    if (throttle >= lastThrottle) {
                        messageSent = true;
                        sendWithTick(new TickTurbo(secretTurboMessage, carPositionsWrapper.gameTick));
                    }
                }

                /*if (angleMismatch)
                    throttle /= 2;*/
                if (physicsKnown && !messageSent) {
                    if (throttle < 1e-3) throttle = 0;
                    sendWithTick(new TickThrottle(throttle, carPositionsWrapper.gameTick));
//                    send(new Throttle(throttle), carPositionsWrapper.gameTick);
                    lastThrottle = throttle;
                    throttles.add(throttle);
                } else if (!messageSent) {
                    sendWithTick(new TickThrottle(1.0, carPositionsWrapper.gameTick));
//                    send(new Throttle(1.0), carPositionsWrapper.gameTick);
                    lastThrottle = 1.0;
                    throttles.add(1.0);
                }

                if (physicsKnown && !anglePhysicsKnown) {
                    //calculate angle physics
                    int res = solver.addEquationEnd(da);
                    logger.print("angle prediction: adding right side - " + res);
                    if (res == -1) {
                        logger.print("Failed to derive angle constants!!!");
                        solver = new GaussSolver(); // try again next time, forgetting all previous data?
                    }
                    if (solver.size() == 4) {
                        logger.print("angle prediction: solving ");
                        double[] x = solver.solve();
                        aF = x[0];
                        aA = x[1];
                        aK = x[2];
                        aL = x[3];
                        logger.print("angle physics predicted: " + aF + " " + aA + " " + aK + " " + aL);
                        predictedDa = da;
                        anglePhysicsKnown = true;
                    }
                }

                predictDa(ourData);

                if (physicsKnown) {
                    if (!anglePhysicsKnown) {
                        if (pieces[ourData.piecePosition.pieceIndex].radius > 0 &&
                                ourData.piecePosition.lane.startLaneIndex == ourData.piecePosition.lane.endLaneIndex) {
                            logger.print("angle prediction: adding full left side");
                            double bend = pieces[ourData.piecePosition.pieceIndex].angle;
                            double sign = Math.signum(bend);
                            double R = calcRadius(ourData);
                            // if you change something here, you must change it for drift
                            solver.addEquation(da, predictedSpeed * ourData.angle, sign * predictedSpeed ,
                                    sign * predictedSpeed * predictedSpeed / R);
                        } else {
                            logger.print("angle prediction: adding short left side");
                            solver.addEquation(da, predictedSpeed * ourData.angle, 0, 0);
                        }
                    }
                    if (isTurboIteration(carPositionsWrapper.gameTick))
                        predictedSpeed = lastThrottle * getTurboFactor(carPositionsWrapper.gameTick) / mass + predictedSpeed * (1 - friction / mass);
                    else
                        predictedSpeed = lastThrottle / mass + predictedSpeed * (1 - friction / mass);
                }

                if (socket.getInputStream().available() > 0) {
                    logger.print("Wasted tick " + carPositionsWrapper.gameTick);
                    tickWasted = true;
                } else tickWasted = false;
            } else if (msgFromServer.msgType.equals("join")) {
                logger.print("Received join from server");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                logger.print("Received gameInit from server");
                sendWithTick(new TickThrottle(1.0, 0));
                final GameInitWrapper gameInitWrapper = gson.fromJson(line, GameInitWrapper.class);
                logger.print("Init game state");
                initGame(gameInitWrapper);
                logger.print("Game state init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                logger.print("Received gameEnd from server");
                logger.print("positions: " + Arrays.toString(positions.toArray(new Double[]{})));
                logger.print("throttles: " + Arrays.toString(throttles.toArray(new Double[]{})));
            } else if (msgFromServer.msgType.equals("gameStart")) {
                logger.print("Received gameStart from server");
                sendWithTick(new TickThrottle(1.0, 0));
            } else if (msgFromServer.msgType.equals("yourCar")) {
                logger.print("Received yourCar from server");
                final YourCarWrapper yourCarWrapper = gson.fromJson(line, YourCarWrapper.class);
                ourCar = yourCarWrapper.data.clone();
            } else if (msgFromServer.msgType.equals("crash")) {
                final CrashWrapper crashWrapper = gson.fromJson(line, CrashWrapper.class);
                if (crashWrapper.data.equals(ourCar)) {
                    logger.print("!!!!!!!!!!WE HAVE CRASHED!!!!!!!!!");
                    crashed = true;
                }
            } else if (msgFromServer.msgType.equals("spawn")) {
                final SpawnWrapper spawnWrapper = gson.fromJson(line, SpawnWrapper.class);
                if (spawnWrapper.data.equals(ourCar)) {
                    logger.print("!!!!! WE HAVE SPAWNED!!!!");
                    crashed = false;
                }
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                turboAvailable = true;
                final TurboAvailableWrapper turboAvailableWrapper = gson.fromJson(line, TurboAvailableWrapper.class);
                turboData = turboAvailableWrapper.data;
                logger.print("we got turbo! duration = " + turboData.turboDurationMilliseconds
                        + " durationTicks = " + turboData.turboDurationTicks + " factor " + turboData.turboFactor);
            } else if (msgFromServer.msgType.equals("turboStart")) {
                if (turboAvailable) {
                    turboAvailable = false;
                    final TurboStartWrapper turboStartWrapper = gson.fromJson(line, TurboStartWrapper.class);
                    if (turboStartWrapper.data.equals(ourCar)) {
                        turboStart = turboStartWrapper.gameTick;
                        turboEnd = turboStart + (int) Math.round(turboData.turboDurationTicks);
                        logger.print("Turbo started at " + turboStart + " till " + turboEnd);
                    } else logger.print("not our car got turbo");
                }
            } else if (msgFromServer.msgType.equals("createRace")) {
                logger.print("Received createRace from server");
            } else if (msgFromServer.msgType.equals("joinRace")) {
                logger.print("Received joinRace from server");
            } else {
                //do nothing, message is already printed
            }
        }

        logger.close();
        mapLogger.close();
        gameLogger.close();
        pathFinderLogger.close();
    }

    public static void main(String... args) throws IOException, CloneNotSupportedException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String raceType = "join";
        String password = null;
        String trackName = null;
        int carCount = 0;
        if (args.length > 4) {
            raceType = args[4];
            password = args[5];
            trackName = args[6];
            carCount = Integer.parseInt(args[7]);
        }
        logger = new Logger(botName, "info.log");
        mapLogger = new Logger(botName, "map.log");
        gameLogger = new Logger(botName, "game.log");
        pathFinderLogger = new Logger(botName, "pathFinder.log");
        logger.print("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, socket, writer, botName, botKey, raceType, password, trackName, carCount);
    }

    boolean IsGoodPieceForTurbo(int piece) {
        logger.print("IsGoodPieceForTurbo: piece = " + piece + " mapName = " + mapName);
        if (mapName.equals("germany")) {
            return piece == 54;
        } else if (mapName.equals("keimola")) {
            return piece == 34;
        } else if (mapName.equals("usa"))
            return piece == 11 || (piece == 27 && optimalPathPieces.size() - currentPtr < pieces.length);
        else return false;
    }

    boolean isTurboIteration(int tick) {
        return tick >= turboStart && tick < turboEnd;
    }

    double getTurboFactor(int tick) {
        return turboData.turboFactor;
    }

    private void predictDa(CarData ourData) {
        double drift = 0;
        suggestR3 = 0; // da' = R2 + R1/sqrt(R)
        suggestR2 = 0; // R = R3 = sqr(R1 / (da'-R2));
        suggestR1 = 0;
        double R = calcRadius(ourData);
        if (pieces[ourData.piecePosition.pieceIndex].radius > 0) {// drift force is there if not in straight road
            drift = (aK + aL * predictedSpeed / R) * predictedSpeed;
            suggestR3 = calcRadius(ourData);
            suggestR2 = aK * predictedSpeed;
            suggestR1 = aL * predictedSpeed * predictedSpeed;
        }
        if (drift < 0) {
            drift = 0; // can't be negative
            suggestR1 = 0;
            suggestR2 = 0;
        }
        if (pieces[ourData.piecePosition.pieceIndex].angle < 0) {// drift force is applied in correct direction
            drift = -drift;
            suggestR2 = -suggestR2;
            suggestR1 = -suggestR1;
        }
        suggestR2 += aF * predictedDa + aA * ourData.angle * predictedSpeed;
        predictedDa = aF * predictedDa + aA * ourData.angle * predictedSpeed + drift;

    }

    private void fixSpeed(int prevPiece, int tick) {
        double delta = predictedSpeed - speed;
//        logger.print("prevPiece = " + prevPiece + " currentPtr = " + currentPtr);
        if (prevPiece == currentPtr - 1 && optimalPathLanes.get(currentPtr - 1) != optimalPathLanes.get(currentPtr)) { //adjust length of last piece.
            logger.print("fixed dist of " + optimalPathPieces.get(currentPtr - 1) + " from " +
                    pathFinder.distCache[optimalPathPieces.get(currentPtr - 1)]
                            [optimalPathLanes.get(currentPtr - 1)]
                            [optimalPathLanes.get(currentPtr)]);
            pathFinder.distCache[optimalPathPieces.get(currentPtr - 1)]
                    [optimalPathLanes.get(currentPtr - 1)]
                    [optimalPathLanes.get(currentPtr)] += delta;
            logger.print("fixed dist of " + optimalPathPieces.get(currentPtr - 1) + " to " +
                    pathFinder.distCache[optimalPathPieces.get(currentPtr - 1)]
                            [optimalPathLanes.get(currentPtr - 1)]
                            [optimalPathLanes.get(currentPtr)] + " by " + delta);
            speed = predictedSpeed;
        } else {
            if (physicsKnown && speed >= 0.0 && speed <= 1.0 / friction * (isTurboIteration(tick) ? 1.0 : getTurboFactor(tick))) {
                predictedSpeed = speed;
                logger.print("Speed prediction failed");
            } else {
                speed = predictedSpeed;
                logger.print("Bamp detected");
            }
        }
    }

    private void initGame(GameInitWrapper gameInitWrapper) {
        turboAvailable = false;
        turboStart = -1;
        turboEnd = -1;
        totalDistance = 0;
        positions = new ArrayList<Double>();
        throttles = new ArrayList<Double>();
        isQualification = false;
        crashed = false;
        optimalPathCalculated = false;
        pathFinder = null;
        tickWasted = false;

        if (gameInitWrapper.data.race.raceSession.laps == 0) {
            logger.print("We got zero laps! This is qualification maybe");
            isQualification = true;
            gameInitWrapper.data.race.raceSession.laps = MAX_LAPS;
        }

        laps = gameInitWrapper.data.race.raceSession.laps;

        logger.print("Dumping map");
        mapLogger.print(gameInitWrapper.data.race.toString());
        logger.print("Map dumped");

        mapName = gameInitWrapper.data.race.track.id;

        logger.print("mapname: " + mapName);

        pieces = gameInitWrapper.data.race.track.pieces.toArray(new Piece[]{});
        lanes = gameInitWrapper.data.race.track.lanes.toArray(new Lane[]{});
        carLength = gameInitWrapper.data.race.cars.get(0).dimensions.length;

        logger.print("Calculating optimal paths");
        pathFinder = new OptimalPathFinder(gameInitWrapper, pathFinderLogger);
        pathFinder.dump();
        logger.print("Optimal paths calculated");
    }

    private void calculateOptimalPath(PiecePosition piecePosition) {
        if (pathFinder == null) {
            logger.print("Variable pathFinder is null");
            throw new AssertionError();
        }
        int[] optimalEnds = pathFinder.calcOptimalEnds(piecePosition.lane.startLaneIndex);
        if (optimalEnds == null) {
            logger.print("Variable optimalEnds is null");
            throw new AssertionError();
        }
        optimalPathCalculated = true;
        logger.print("Optimal ends calculated: " + Arrays.toString(optimalEnds));
        logger.print("Getting optimal path");
        getOptimalPath(pathFinder, optimalEnds, piecePosition);
        // calcPrefixSums(pathFinder);

    }

    private void initAngleConstants() {
        aF = 0.9;
        aA = -0.00125;
        aK = -0.3;
        aL = 0.530330086;
    }

    double calcCriticalSpeed(State cur) {
        double maxSpeed = 1000000.0;
        if (pieces[optimalPathPieces.get(cur.i)].radius > 0) {
            maxSpeed = -aK / aL * calcRadius(cur);
        }
        int n = optimalPathPieces.size();
        double curDist = calcLength(cur.i) - cur.x;
        for (int j = cur.i + 1; j < n; j++) {
            if (pieces[optimalPathPieces.get(j)].radius > 0) {
                double R1 = calcRadius(optimalPathPieces.get(j), 0,
                        optimalPathLanes.get(j), optimalPathLanes.get(j));
                double R2 = calcRadius(optimalPathPieces.get(j), 0,
                        optimalPathLanes.get(j + 1), optimalPathLanes.get(j + 1));
                double curSpeed = -aK / aL * /*Math.sqrt*/(Math.min(R1, R2)); // dirty fix. may need mean, max or min here
                curSpeed += curDist * friction / mass;
                if (curSpeed < maxSpeed) maxSpeed = curSpeed;
            }
            curDist += calcLength(j);
        }
        return maxSpeed;
    }

    double calcCriticalSpeedNoAngle(State cur) {
        double dist = 0;
        dist = calcLength(cur.i) - cur.x;
        for (int j = cur.i + 1; j < optimalPathPieces.size(); j++) {
            dist += calcLength(j);
            if (pieces[optimalPathPieces.get(j)].radius > 0) {
                break;
            }
        }
        return 1.0;
    }


    boolean Simulate(State init, double throttle0, double minSpeed) {
        State cur = init.next(throttle0);
        int it = 0;
        while (cur.i < optimalPathPieces.size()) {
            if (Math.abs(cur.a) >= BAD_ANGLE)
                return false;
            if (it > MAX_SIMULATE_ITERATIONS) break;
            double maxAllowedSpeed = calcCriticalSpeed(cur);
            double tr = calcThrottle(cur.v, maxAllowedSpeed);
            //tr = Math.max(tr, calcThrottle(cur.v, minSpeed));
            cur.nextInPlace(tr);
            it++;
        }
        return true;
    }

    // id in optimal path
    private double calcLength(int i) {
        return pathFinder.distCache[optimalPathPieces.get(i)][optimalPathLanes.get(i)][optimalPathLanes.get(i + 1)];
    }

    private double calcRadius(CarData ourData) {
        return calcRadius(ourData.piecePosition.pieceIndex, ourData.piecePosition.inPieceDistance,
                ourData.piecePosition.lane.startLaneIndex, ourData.piecePosition.lane.endLaneIndex);
    }

    private double calcRadius(State st) {
        return calcRadius(optimalPathPieces.get(st.i), st.x, optimalPathLanes.get(st.i), optimalPathLanes.get(st.i + 1));
    }

    private double calcRadius(int piece, double dist, int l1, int l2) {
        double r1 = pieces[piece].radius - Math.signum(pieces[piece].angle) * lanes[l1].distanceFromCenter;
        if (l1 != l2) {
            double r2 = pieces[piece].radius - Math.signum(pieces[piece].angle) * lanes[l2].distanceFromCenter;
            r1 += (r2 - r1) * dist / pathFinder.distCache[piece][l1][l2];
        }
        return r1;
    }

    private int getSwitchDirection(int pieceIndex, int lap, int gameTick) {
        if (gameTick == 0) {
            return 0;
        }
        logger.print("getSwitchDirection pieceIndex = " + pieceIndex + " lap = " + lap);
        update(pieceIndex);
        logger.print("after update currentPtr = " + currentPtr);
        if (currentPtr + 2 < optimalPathPieces.size() &&
                optimalPathLanes.get(currentPtr + 1) != optimalPathLanes.get(currentPtr + 2)) {
            logger.print("lane1 = " + optimalPathLanes.get(currentPtr + 1) +
                    " lane2 = " + optimalPathLanes.get(currentPtr + 2) + " switchSent = " + switchSent[currentPtr + 1]);
            if (!switchSent[currentPtr + 1]) {
                logger.print("returning " + (optimalPathLanes.get(currentPtr + 2) - optimalPathLanes.get(currentPtr + 1)));
                return optimalPathLanes.get(currentPtr + 2) - optimalPathLanes.get(currentPtr + 1);
            } else {
                logger.print("returning 0");
                return 0;
            }
        }
        return 0;
    }

    void update(int curPieceId) {
        logger.print("update: pieceId = " + curPieceId + " ptr = " + currentPtr);
        while (currentPtr + 1 < optimalPathPieces.size() && optimalPathPieces.get(currentPtr) != curPieceId) {
            currentPtr++;
            if (currentPtr < optimalPathLanes.size())
                totalDistance += pathFinder.distCache[optimalPathPieces.get(currentPtr - 1)]
                        [optimalPathLanes.get(currentPtr - 1)]
                        [optimalPathLanes.get(currentPtr)];
        }
    }

//    private void calcPrefixSums(OptimalPathFinder pathFinder) {
//        logger.print("Calculating prefix sums");
//        prefixSums = new double[optimalPathPieces.size()];
//        prefixSums[0] = 0;
//        for (int i = 0; i < prefixSums.length - 1; i++) {
//            int curPieceId = optimalPathPieces.get(i);
//            int curLaneId = optimalPathLanes.get(i);
//            int nextLaneId = optimalPathLanes.get(i + 1);
//            prefixSums[i + 1] = prefixSums[i] + pathFinder.distCache[curPieceId][curLaneId][nextLaneId];
//        }
//        logger.print("prefixSums : " + Arrays.toString(prefixSums));
//    }

    private void getOptimalPath(OptimalPathFinder pathFinder, int[] optimalEnds, PiecePosition piecePosition) {
        currentPtr = 0;
        optimalPathPieces = new ArrayList<Integer>();
        optimalPathLanes = new ArrayList<Integer>();
        if (piecePosition.pieceIndex != 0) {
            optimalPathPieces.add(piecePosition.pieceIndex);
            optimalPathLanes.add(piecePosition.lane.startLaneIndex);
        }
        int curLaneId = piecePosition.lane.startLaneIndex;
        for (int lap = 0; lap < laps; lap++) {
            int pieceId = 0;
            while (true) {
                int next = pathFinder.getNext(pieceId, curLaneId, optimalEnds[lap]);
                if (next == -2) {
                    break;
                }
                optimalPathPieces.add(pieceId);
                optimalPathLanes.add(curLaneId);
                pieceId++;
                curLaneId += next;
            }
        }
        optimalPathLanes.add(optimalEnds[laps - 1]);
        switchSent = new boolean[optimalPathLanes.size()];
        Arrays.fill(switchSent, false);
        String optimalPath = "\n";
        for (int i = 0; i < optimalPathPieces.size(); i++) {
            optimalPath += "(" + optimalPathPieces.get(i) + ", " + optimalPathLanes.get(i) + ")\n";
        }
        logger.print("optimal path:" + optimalPath);
    }


    double BinSearchFirstThrottle(State init, double minSpeed) {
        double mn, mx;
        mn = calcThrottle(init.v, calcCriticalSpeed(init));
        if (!Simulate(init, mn, minSpeed)) {
            logger.print("We are doomed to fail on iteration " + init.iteration + " even with throttle " + mn);
            mn = 0.0;
        }
        mx = 1.0;
        if (Simulate(init, mx, minSpeed)) return mx;
        for (int it = 0; it < MAX_BINSEARCH_ITERATIONS; it++) {
            double mid = (mn + mx) / 2.0;
            if (Simulate(init, mid, minSpeed))
                mn = mid;
            else mx = mid;
        }
        return mn;
    }

    State PredictTravel(State init, double minSpeed) {
        double ans = 0.0;
        State cur = new State(init.x, init.v, init.a, init.da, init.i, init.iteration);
        for (int i = 0; i < MAX_PREDICT_ITERATIONS; i++) {
            double tr = BinSearchFirstThrottle(init, minSpeed);
            cur.nextInPlace(tr);
            if (cur.i > optimalPathPieces.size()) break;
        }
        return cur;
    }

    private double getThrottle(CarData ourData, int gameTick) {
        logger.print("getThrottle: pieceId = " + ourData.piecePosition.pieceIndex + " gameTick = " + gameTick);
        // HERE IS SOME FUCKING MAGIC
        if (gameTick < 3 || !physicsKnown) {
            return 1.0;
        }
        State init = new State(ourData.piecePosition.inPieceDistance, predictedSpeed,
                ourData.angle, predictedDa, currentPtr, gameTick);
        if (!anglePhysicsKnown) {
            return calcCriticalSpeedNoAngle(init);
        }
        return BinSearchFirstThrottle(init, 0);
        /*
        double minSpeed = 0;
        double L, R;
        L = 0; R = 1.0 / friction;
        if (init.v < calcCriticalSpeed(init))
            return BinSearchFirstThrottle(init, 0);; //do not calculate all the stuff, if we can accelerate now.
        for (int i = 0; i < MAX_BINSEARCH_ITERATIONS; i++) {
            double m1, m2;
            m1 = (L+L+R)/3.0;
            m2 = (L+R+R)/3.0;
            State s1 = PredictTravel(init, m1);
            State s2 = PredictTravel(init, m2);
            if (s1.i > s2.i || (s1.i == s2.i && s1.x > s2.x))
                R = m1;
            else
                L = m2;
        }
        logger.print("Speed threshold = ( " + L + " " + R +" ) ");
        return BinSearchFirstThrottle(init, R);*/
    }

    private double calcThrottle(double v0, double v1) {
        double ret = Math.min(1.0, mass * v1 - (mass - friction) * v0);
        if (ret < 0)
            ret = 0;
        return ret;
    }

    private void calcPhysicsConstants() {
        zeroCoordinate = positions.get(0);
        firstCoordinate = positions.get(1);
        secondCoordinate = positions.get(2);
        double x1 = firstCoordinate - zeroCoordinate;
        double x2 = secondCoordinate - zeroCoordinate;
        if (x1 > 0 && x2 > x1) {
            friction = (3 * x1 - x2) / (x1 * x1);
            mass = 1 / x1;
            double k = (1 - friction / mass);
            predictedSpeed = 0;
            for (int tick = 0; tick < 2; tick++)
                predictedSpeed = predictedSpeed * k + 1 / mass;
            assert (predictedSpeed == x2 - x1);
            physicsKnown = true;
            logger.print("physics: " + "friction = " + friction + " mass = " + mass);
        } else {
            zeroCoordinate = firstCoordinate;
            firstCoordinate = secondCoordinate;
        }
    }

    private String getDirection(int dir) {
        logger.print(dir + "");
        return dir == -1 ? "Left" : "Right";
    }

    private CarData findCar(ArrayList<CarData> data, CarId ourCar) {
        for (CarData carData : data) {
            if (carData.id.equals(ourCar)) {
                return carData;
            }
        }
        return null;
    }

    private void send(final SendMsg msg) {
        logger.print("Sending: " + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
        logger.print("Message sent");
    }

    private void sendWithTick(final SendTickMsg msg) {
        logger.print("Sending: " + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
        logger.print("Message sent");
    }

    class State {
        public double x;
        public double v;
        public double a;
        public double da;
        int i;
        int iteration;

        State(double x, double v, double a, double da, int i, int it) {
            this.x = x;
            this.v = v;
            this.a = a;
            this.da = da;
            this.i = i;
            this.iteration = it;
        }

        State next(double tr) {
            State tmp = new State(x, v, a, da, i, iteration);
            tmp.nextInPlace(tr);
            return tmp;
        }

        void nextInPlace(double tr) {
            double R = calcRadius(this);
            double alpha = pieces[optimalPathPieces.get(i)].angle;
            double drift = 0;
            if (R > 0) drift = aK * v  + aL * v * v / R;
            if (drift < 0) drift = 0;
            if (alpha < 0) drift = -drift;
            da = aF * da + aA * v * a + drift;
            a = a + da;
            if (isTurboIteration(iteration))
                v = (1 - friction / mass) * v + getTurboFactor(iteration) * tr / mass;
            else
                v = (1 - friction / mass) * v + tr / mass;
            x = x + v;
            iteration++;
            while (i < optimalPathPieces.size() &&
                    x > pathFinder.distCache[optimalPathPieces.get(i)]
                            [optimalPathLanes.get(i)]
                            [optimalPathLanes.get(i + 1)]) {
                x -= pathFinder.distCache[optimalPathPieces.get(i)]
                        [optimalPathLanes.get(i)]
                        [optimalPathLanes.get(i + 1)];
                i++;
            }
        }
    }

//    private void send(final SendMsg msg, int tick) {
//        String str = msg.toJson();
//        str = str.substring(0, str.length()-1) + ", \"gameTick\": " + tick + "}";
//        logger.print("Sending: " + str);
//        writer.println(str);
//        writer.flush();
//        logger.print("Message sent");
//    }
}

class MsgTickWrapper {
    public final String msgType;
    public final Object data;
    public final int gameTick;

    public MsgTickWrapper(String msgType, Object data, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgTickWrapper(final SendTickMsg sendTickMsg) {
        this(sendTickMsg.msgType(), sendTickMsg.msgData(), sendTickMsg.gameTick);
    }
}

abstract class SendTickMsg {
    protected int gameTick;

    public String toJson() {
        return new Gson().toJson(new MsgTickWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class TickThrottle extends SendTickMsg {
    protected double throttle;

    TickThrottle(double throttle, int gameTick) {
        this.gameTick = gameTick;
        this.throttle = throttle;
    }

    @Override
    protected Object msgData() {
        return throttle;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class TickTurbo extends SendTickMsg {
    protected String turboMessage;

    TickTurbo(String turboMessage, int gameTick) {
        this.gameTick = gameTick;
        this.turboMessage = turboMessage;
    }

    @Override
    protected Object msgData() {
        return turboMessage;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}

class TickSwitchLane extends SendTickMsg {
    protected String direction;

    TickSwitchLane(String direction, int gameTick) {
        this.gameTick = gameTick;
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class BotId {
    protected String name;
    protected String key;

    BotId(String name, String key) {
        this.name = name;
        this.key = key;
    }
}

class Turbo extends SendMsg {
    protected String turboMessage;

    Turbo(String turboMessage) {
        this.turboMessage = turboMessage;
    }

    @Override
    protected Object msgData() {
        return turboMessage;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}

class SwitchLane extends SendMsg {
    protected String direction;

    SwitchLane(String direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class JoinRace extends SendMsg {
    protected BotId botId;
    protected String trackName;
    protected String password;
    protected int carCount;

    JoinRace(BotId botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class CreateRace extends SendMsg {
    protected BotId botId;
    protected String trackName;
    protected String password;
    protected int carCount;

    CreateRace(BotId botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}