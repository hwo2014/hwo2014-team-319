package noobbot;

class TurboData {
    protected double turboDurationMilliseconds;
    protected double turboDurationTicks;
    protected double turboFactor;

    TurboData(double turboDurationMilliseconds, double turboDurationTicks, double turboFactor) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
        this.turboDurationTicks = turboDurationTicks;
        this.turboFactor = turboFactor;
    }
}

class TurboAvailableWrapper {
    protected String msgType;
    protected TurboData data;

    public TurboAvailableWrapper(String msgType, TurboData data) {
        this.msgType = msgType;
        this.data = data;
    }
}

class TurboStartWrapper {
    protected String msgType;
    CarId data;
    int gameTick;

    public TurboStartWrapper(String msgType, CarId data, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }
}