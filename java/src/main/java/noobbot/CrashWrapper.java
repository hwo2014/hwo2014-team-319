package noobbot;

class CrashWrapper {
    protected String msgType;
    protected CarId data;
    protected String gameId;
    protected int gameTick;

    CrashWrapper(String msgType, CarId data, String gameId, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }
}
