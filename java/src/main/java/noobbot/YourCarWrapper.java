package noobbot;

public class YourCarWrapper {
    protected String msgType;
    protected CarId data;

    public YourCarWrapper(String msgType, CarId data) {
        this.msgType = msgType;
        this.data = data;
    }
}
