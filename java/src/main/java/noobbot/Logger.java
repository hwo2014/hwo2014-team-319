package noobbot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Logger{
    PrintWriter log;

    Logger(String botName, String logName) {
        try {
            String dateStamp = getDateStamp();
            String timeStamp = getTimeStamp().replaceAll(":", "_");
            String path = "logs/" + dateStamp + "/" + botName + "/" + timeStamp;
            new File(path).mkdirs();
            log = new PrintWriter(path + "/" + logName);
        } catch (FileNotFoundException e) {
            System.err.println("Can't create a file for logger");
            e.printStackTrace();
        }
    }

    public void print(String message) {
        String tmp = getTimeStamp() + "|" + message;
        System.out.println(tmp);
        log.println(tmp);
        log.flush();
    }

    public void close() {
        log.close();
    }

    private String getDateStamp() {
        return new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
    }

    private String getTimeStamp() {
        return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}
