package noobbot;

class SpawnWrapper {
    protected String msgType;
    protected CarId data;
    protected String gameId;
    protected int gameTick;

    public SpawnWrapper(String msgType, CarId data, String gameId, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }
}
