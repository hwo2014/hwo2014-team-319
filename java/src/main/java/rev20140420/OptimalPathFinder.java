package rev20140420;

import java.util.ArrayList;

public class OptimalPathFinder {
    public final static double EPS = 1e-9;
    Logger logger;
    ArrayList<Piece> pieces;
    ArrayList<Lane> lanes;
    double[][][] bestLength;
    double[][][] distCache;
    int[][][] next;
    int laps;

    public OptimalPathFinder(GameInitWrapper gameInitWrapper, Logger logger) {
        this.logger = logger;
        this.pieces = (ArrayList<Piece>) gameInitWrapper.data.race.track.pieces.clone();
        this.lanes = (ArrayList<Lane>) gameInitWrapper.data.race.track.lanes.clone();
        bestLength = new double[pieces.size() + 1][lanes.size()][lanes.size()];
        next = new int[pieces.size() + 1][lanes.size()][lanes.size()];
        this.laps = gameInitWrapper.data.race.raceSession.laps;
        distCache = new double[pieces.size()][lanes.size()][lanes.size()];
        calcIt();
    }

    public static int compare(double a, double b) {
        if (Math.abs(a - b) < EPS) {
            return 0;
        }
        return a < b ? -1 : 1;
    }

    private void calcIt() {
        calcAllDist();
        int n = pieces.size();
        int m = lanes.size();
        for (int needLane = 0; needLane < m; needLane++) {
            for (int ourLane = 0; ourLane < m; ourLane++) {
                bestLength[n][ourLane][needLane] = Double.POSITIVE_INFINITY;
                next[n][ourLane][needLane] = -2;
            }
            bestLength[n][needLane][needLane] = 0;
        }
        for (int pieceId = n - 1; pieceId >= 0; pieceId--) {
            for (int ourLaneId = 0; ourLaneId < m; ourLaneId++) {
                for (int needLaneId = 0; needLaneId < m; needLaneId++) {
                    Piece piece = pieces.get(pieceId);
                    Lane lane = lanes.get(ourLaneId);
                    double curBest = bestLength[pieceId + 1][ourLaneId][needLaneId]
                            + distCache[pieceId][ourLaneId][ourLaneId];
                    int curNext = 0;
                    if (piece.isSwitch) {
                        if (ourLaneId > 0) {
                            double val = bestLength[pieceId + 1][ourLaneId - 1][needLaneId]
                                    + distCache[pieceId][ourLaneId][ourLaneId - 1];
                            if (compare(val, curBest) < 0) {
                                curBest = val;
                                curNext = -1;
                            }
                        }
                        if (ourLaneId + 1 < m) {
                            double val = bestLength[pieceId + 1][ourLaneId + 1][needLaneId]
                                    + distCache[pieceId][ourLaneId][ourLaneId + 1];
                            if (compare(val, curBest) < 0) {
                                curBest = val;
                                curNext = 1;
                            }
                        }
                    }
                    bestLength[pieceId][ourLaneId][needLaneId] = curBest;
                    next[pieceId][ourLaneId][needLaneId] = curNext;
                }
            }
        }
    }

    private void calcAllDist() {
        for (int pieceId = 0; pieceId < pieces.size(); pieceId++) {
            Piece piece = pieces.get(pieceId);
            for (int ourLaneId = 0; ourLaneId < lanes.size(); ourLaneId++) {
                Lane lane = lanes.get(ourLaneId);
                distCache[pieceId][ourLaneId][ourLaneId] = calcDist(piece, lane);
                if (ourLaneId > 0) {
                    distCache[pieceId][ourLaneId][ourLaneId - 1] = calcSwitchDist(piece, lane, lanes.get(ourLaneId - 1));
                }
                if (ourLaneId + 1 < lanes.size()) {
                    distCache[pieceId][ourLaneId][ourLaneId + 1] = calcSwitchDist(piece, lane, lanes.get(ourLaneId + 1));
                }
            }
        }
    }

    public void dump() {
        logger.print("Dumping bestLength");
        {
            String ret = "\n";
            for (int pieceId = 0; pieceId <= pieces.size(); pieceId++) {
                for (int ourLane = 0; ourLane < lanes.size(); ourLane++) {
                    for (int needLane = 0; needLane < lanes.size(); needLane++) {
                        ret += bestLength[pieceId][ourLane][needLane] + " ";
                    }
                    ret += "\n";
                }
                ret += "====================\n";
            }
            logger.print(ret);
        }
        logger.print("bestLength dumped");
        logger.print("Dumping next");

        {
            String ret = "\n";
            for (int pieceId = 0; pieceId <= pieces.size(); pieceId++) {
                for (int ourLane = 0; ourLane < lanes.size(); ourLane++) {
                    for (int needLane = 0; needLane < lanes.size(); needLane++) {
                        ret += next[pieceId][ourLane][needLane] + " ";
                    }
                    ret += "\n";
                }
                ret += "====================\n";
            }
            logger.print(ret);
        }
        logger.print("next dumped");
    }

    public int[] calcOptimalEnds(int startLane) {
        double[][] bestPath = new double[laps + 1][lanes.size()];
        int[][] previous = new int[laps + 1][lanes.size()];
        for (int laneId = 0; laneId < lanes.size(); laneId++) {
            bestPath[0][laneId] = laneId == startLane ? 0 : Double.POSITIVE_INFINITY;
        }
        for (int curLap = 1; curLap <= laps; curLap++) {
            for (int curLane = 0; curLane < lanes.size(); curLane++) {
                bestPath[curLap][curLane] = Double.POSITIVE_INFINITY;
                for (int prevLane = 0; prevLane < lanes.size(); prevLane++) {
                    double val = bestPath[curLap - 1][prevLane] + bestLength[0][prevLane][curLane];
                    if (compare(bestPath[curLap][curLane], val) > 0) {
                        bestPath[curLap][curLane] = val;
                        previous[curLap][curLane] = prevLane;
                    }
                }
            }
        }
        int lastLane = 0;
        for (int laneId = 1; laneId < lanes.size(); laneId++) {
            if (compare(bestPath[laps][lastLane], bestPath[laps][laneId]) > 0) {
                lastLane = laneId;
            }
        }
        int[] ret = new int[laps];
        for (int curLap = laps;  curLap > 0; curLap--) {
            ret[curLap - 1] = lastLane;
            lastLane = previous[curLap][lastLane];
        }
        return ret;
    }

    public int getNext(int pieceId, int ourLaneId, int needLaneId) {
        if (pieceId == pieces.size()) {
            return -2;
        }
        return next[pieceId][ourLaneId][needLaneId];
    }

    private double calcSwitchDist(Piece piece, Lane lane, Lane nextLane) {
        if (compare(piece.length, 0.0) == 0) {
            RoundCurve roundCurve = new RoundCurve(piece.radius - Math.signum(piece.angle) * lane.distanceFromCenter,
                    piece.radius - Math.signum(piece.angle) * nextLane.distanceFromCenter,
                    Math.abs(piece.angle) * Math.PI / 180.0);
            //double ret = Integrator.integrateSimpson(roundCurve, 0.0, 1.0);
            double ret = roundCurve.g(Math.abs(piece.angle) * Math.PI / 180.0) - roundCurve.g(0);
            logger.print("Distance for " + piece.toString() + " " + lane.toString()
                    + " " + nextLane.toString() + " = " + ret);
            return ret;
        } else {
            StraightCurve straightCurve = new StraightCurve(piece.length,
                    Math.abs(lane.distanceFromCenter - nextLane.distanceFromCenter));
            double ret = Integrator.integrateSimpson(straightCurve, 0.0, 1.0);
            logger.print("Distance for " + piece.toString() + " " + lane.toString()
                    + " " + nextLane.toString() + " = " + ret);
            return ret;
        }
    }

    private double calcDist(Piece piece, Lane lane) {
        if (compare(piece.length, 0.0) == 0) {
            double angle = Math.PI * piece.angle / 180.0;
            return Math.abs(angle) * piece.radius - lane.distanceFromCenter * angle;
        } else {
            return piece.length;
        }
    }

}
