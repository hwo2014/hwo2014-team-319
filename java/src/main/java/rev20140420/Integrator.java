package rev20140420;

abstract class Function {
    abstract double f(double x);
}

class StraightCurve extends Function {
    private double L;
    private double W;

    StraightCurve(double l, double w) {
        L = l;
        W = w;
    }

    @Override
    double f(double t) {
        double xx = L / 2 * (6 * t * t - 6 * t + 3);
        double yy = W * (6 * t - 6 * t * t);
        return Math.sqrt(xx * xx + yy * yy);
    }
}

class RoundCurve extends Function {
    private double r0;
    private double r1;
    private double a;

    RoundCurve(double r0, double r1, double a) {
        this.r0 = r0;
        this.r1 = r1;
        this.a = a;
    }

    @Override
    double f(double t) {
        double xx = 2 * t * (r0 + r1 * Math.cos(a) - (r0 + r1) * Math.cos(a / 2))
                + (r0 + r1) * Math.cos(a / 2) - 2 * r0;
        double yy = 2 * t * (r1 * Math.sin(a) - (r0 + r1) * Math.sin(a / 2))
                + (r0 + r1) * Math.sin(a / 2);
        return Math.sqrt(xx * xx + yy * yy);
    }

    double g (double x) {
        double B = (r1 - r0) / a;
        double C = r0;
        double A = B * B;

        double sq = Math.sqrt(A + (B * x + C) * (B * x + C));
        double bxc = B * x + C;

        return (bxc * sq + A * Math.log(sq + bxc)) / (2 * B);
    }
}

public class Integrator {

    static double integrateSimpson(Function function, double a, double b) {
        final int ITERATIONS = 1000;
        double h = (b - a) / ITERATIONS;
        double ret = 0;
        double x1 = a;
        double x2 = a + h / 2;
        double x3 = a + h;
        for (int i = 0; i < ITERATIONS; i++) {
            ret += (function.f(x1) + 4 * function.f(x2) + function.f(x3)) * h / 6;
            x1 += h;
            x2 += h;
            x3 += h;
        }
        return ret;
    }
}
