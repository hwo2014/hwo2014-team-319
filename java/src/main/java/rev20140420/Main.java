package rev20140420;

import com.google.gson.Gson;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    final static int MAX_LAPS = 123;

    static Logger logger;
    static Logger mapLogger;
    static Logger gameLogger;
    static Logger pathFinderLogger;
    final Gson gson = new Gson();
    private PrintWriter writer;

    // TODO : make it OK
    boolean hasStarted;
    double lastPosition;
    double lastAngle;
    double friction;
    double mass;
    double zeroCoordinate;
    double firstCoordinate;
    double secondCoordinate;
    double lastThrottle;
    double totalDistance;
    double speed; // current speed
    double predictedSpeed; // predicted speed
    double predictedDa; // predicted angle speed
    double da; // current angle speed
    double carLength;
    double aF; // angle speed equaton parameters
    double aA;
    double aK;
    double aL;

    boolean physicsKnown;
    boolean anglePhysicsKnown;

    int laps;
    Piece[] pieces;
    Lane[] lanes;
    int currentPtr;
    ArrayList<Integer> optimalPathPieces;
    ArrayList<Integer> optimalPathLanes;
    boolean[] switchSent;
    double[] prefixSums;
    double[] maxSpeed;

    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey,
                final String raceType, final String password, final String trackName, final int carCount) throws IOException, CloneNotSupportedException {
        aF = 0.9;
        aA = -0.00125;
        aK = -0.3;
        aL = 0.530330086;
        //anglePhysicsKnown = false;
        anglePhysicsKnown = true;
        this.writer = writer;
        String line = null;
        hasStarted = false;
        physicsKnown = false;
        if (raceType.equals("join")) {
            send(new Join(botName, botKey));
        } else if (raceType.equals("createRace")) {
            if (password == null || trackName == null) {
                logger.print("password or trackName is null");
                throw new AssertionError();
            }
            send(new CreateRace(new BotId(botName, botKey), trackName, password, carCount));
        } else if (raceType.equals("joinRace")) {
            if (password == null || trackName == null) {
                logger.print("password or trackName is null");
                throw new AssertionError();
            }
            send(new JoinRace(new BotId(botName, botKey), trackName, password, carCount));
        } else {
            logger.print("Can't understand what " + raceType + " is");
            throw new AssertionError();
        }

        CarId ourCar = null;
        int lastLap = -1;
        OptimalPathFinder pathFinder = null;
        boolean optimalEndsCalculated = false;
        ArrayList<Double> positions = new ArrayList<Double>();
        ArrayList<Double> throttles = new ArrayList<Double>();
        boolean crashed = false;
        while ((line = reader.readLine()) != null) {
            if (crashed) {
                logger.print("!!!!!!WE HAVE CRASHED!!!!!!");
                predictedSpeed = predictedDa = lastAngle = 0;
            }
            logger.print("Received from server: " + line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                logger.print("Received carPositions from server");
                final CarPositionsWrapper carPositionsWrapper = gson.fromJson(line, CarPositionsWrapper.class);
                CarData ourData = findCar(carPositionsWrapper.data, ourCar);
                if (ourData == null) {
                    // TODO: more informative log here
                    logger.print("Can't find our car");
                    throw new AssertionError();
                }
                if (!optimalEndsCalculated) {
                    logger.print("Calculating optimal ends");
                    if (pathFinder == null) {
                        logger.print("Variable pathFinder is null");
                        throw new AssertionError();
                    }
                    int[] optimalEnds = pathFinder.calcOptimalEnds(ourData.piecePosition.lane.startLaneIndex);
                    if (optimalEnds == null) {
                        logger.print("Variable optimalEnds is null");
                        throw new AssertionError();
                    }
                    optimalEndsCalculated = true;
                    logger.print("Optimal ends calculated: " + Arrays.toString(optimalEnds));
                    logger.print("Getting optimal path");
                    getOptimalPath(pathFinder, optimalEnds, ourData.piecePosition.lane.startLaneIndex);
                    calcPrefixSums(pathFinder);
                    logger.print("Optimal path got");
                }
                logger.print("Dumping tick " + carPositionsWrapper.gameTick);
                gameLogger.print(carPositionsWrapper.gameTick + " " + ourData.piecePosition.pieceIndex
                        + " " + ourData.piecePosition.inPieceDistance + " " + ourData.piecePosition.lane.startLaneIndex
                        + " " + ourData.piecePosition.lane.endLaneIndex + "\t\ta, sp, bend = " + ourData.angle
                        + " " + predictedSpeed + " " + calcRadius(ourData));
                logger.print("Tick " + carPositionsWrapper.gameTick + " dumped");
                if (carPositionsWrapper.gameTick == 0) {
                    zeroCoordinate = ourData.piecePosition.inPieceDistance;
                } else if (carPositionsWrapper.gameTick == 1) {
                    firstCoordinate = ourData.piecePosition.inPieceDistance;
                } else if (carPositionsWrapper.gameTick >= 2 && !physicsKnown) {
                    secondCoordinate = ourData.piecePosition.inPieceDistance;
                    calcPhysicsConstants();
                    if (physicsKnown)
                        calcMaxSpeed(pathFinder);
                }
                if (ourData.piecePosition.lap == 0 && ourData.piecePosition.pieceIndex == 0) {
                    if (!hasStarted) {
                        logger.print("Race has started!");
                        hasStarted = true;
                    }
                }
                int switchDirection = getSwitchDirection(ourData.piecePosition.pieceIndex,
                        ourData.piecePosition.lap, carPositionsWrapper.gameTick);
                if (switchDirection != 0) {
                    send(new SwitchLane(getDirection(switchDirection)));
                }
                update(ourData.piecePosition.pieceIndex);
                double curPosition = ourData.piecePosition.inPieceDistance + totalDistance;
                speed = curPosition - lastPosition;
                lastPosition = curPosition;

                positions.add(curPosition);
                da = ourData.angle - lastAngle;
                lastAngle = ourData.angle;

                logger.print("Tick " + carPositionsWrapper.gameTick + " speed = " + speed + " == " + predictedSpeed +
                        " da = " + da + " == " + predictedDa);
                if (OptimalPathFinder.compare(speed, predictedSpeed) != 0 && carPositionsWrapper.gameTick > 1) {
                    logger.print("!!!! Speed mismatch !!!!");
                    speed = predictedSpeed;
                }

                if (OptimalPathFinder.compare(da, predictedDa) != 0) {
                    logger.print("!!!! angle mismatch !!!!");
                    da = predictedDa;
                }
                if (switchDirection == 0 && physicsKnown) {
                    double throttle = getThrottle(ourData, carPositionsWrapper.gameTick, pathFinder);
                    send(new Throttle(throttle));
                    lastThrottle = throttle;
                    throttles.add(throttle);
                } else if (switchDirection == 0) {
                    send(new Throttle(1.0));
                    lastThrottle = 1.0;
                    throttles.add(1.0);
                }


                if (anglePhysicsKnown) {
                    double drift = 0;
                    if (pieces[ourData.piecePosition.pieceIndex].radius > 0) // drift force is there if not in stright road
                        drift = (aK + aL * predictedSpeed / Math.sqrt(calcRadius(ourData))) * predictedSpeed;
                    if (drift < 0) drift = 0; // can't be negative
                    if (pieces[ourData.piecePosition.pieceIndex].angle < 0) // drift force is applied in correct direction
                        drift = -drift;
                    predictedDa = aF * predictedDa + aA * ourData.angle * predictedSpeed + drift;
                }

                if (physicsKnown) {
                    predictedSpeed = lastThrottle / mass + predictedSpeed * (1 - friction / mass);
                }

            } else if (msgFromServer.msgType.equals("join")) {
                logger.print("Received join from server");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                logger.print("Received gameInit from server");
                final GameInitWrapper gameInitWrapper = gson.fromJson(line, GameInitWrapper.class);
                if (gameInitWrapper.data.race.raceSession.laps == 0) {
                    logger.print("We got zero laps!");
                    gameInitWrapper.data.race.raceSession.laps = MAX_LAPS;
                }
                laps = gameInitWrapper.data.race.raceSession.laps;
                logger.print("Dumping map");
                mapLogger.print(gameInitWrapper.data.race.toString());
                logger.print("Map dumped");
                pieces = gameInitWrapper.data.race.track.pieces.toArray(new Piece[]{});
                lanes = gameInitWrapper.data.race.track.lanes.toArray(new Lane[]{});
                carLength = gameInitWrapper.data.race.cars.get(0).dimensions.length;
                logger.print("Calculating optimal paths");
                pathFinder = new OptimalPathFinder(gameInitWrapper, pathFinderLogger);
                pathFinder.dump();
                pathFinderLogger.close();
                logger.print("Optimal paths calculated");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                logger.print("Received gameEnd from server");
                logger.print("positions: " + Arrays.toString(positions.toArray(new Double[]{})));
                logger.print("throttles: " + Arrays.toString(throttles.toArray(new Double[]{})));
            } else if (msgFromServer.msgType.equals("gameStart")) {
                logger.print("Received gameStart from server");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                logger.print("Received yourCar from server");
                final YourCarWrapper yourCarWrapper = gson.fromJson(line, YourCarWrapper.class);
                ourCar = yourCarWrapper.data.clone();
            } else if (msgFromServer.msgType.equals("crash")) {
                logger.print("!!!!!!!!!!WE HAVE CRASHED!!!!!!!!!");
                crashed = true;
            } else if (msgFromServer.msgType.equals("spawn")) {
                logger.print("!!!!! WE HAVE SPAWNED!!!!");
                crashed = false;
            } else {
                send(new Ping());
            }
        }

        logger.close();
        mapLogger.close();
        gameLogger.close();
        pathFinderLogger.close();
    }

    private double calcRadius(CarData ourData) {
        Piece piece = pieces[ourData.piecePosition.pieceIndex];
        Lane lane = lanes[ourData.piecePosition.lane.startLaneIndex];
        return piece.radius - Math.signum(piece.angle)*lane.distanceFromCenter;
    }

    private int getSwitchDirection(int pieceIndex, int lap, int gameTick) {
        if (gameTick == 0) {
            return 0;
        }
        logger.print("getSwitchDirection pieceIndex = " + pieceIndex + " lap = " + lap);
        update(pieceIndex);
        logger.print("after update currentPtr = " + currentPtr);
        if (currentPtr + 2 < optimalPathPieces.size() &&
                optimalPathLanes.get(currentPtr + 1) != optimalPathLanes.get(currentPtr + 2)) {
            logger.print("lane1 = " + optimalPathLanes.get(currentPtr + 1) +
                    " lane2 = " + optimalPathLanes.get(currentPtr + 2) + " switchSent = " + switchSent[currentPtr + 1]);
            if (!switchSent[currentPtr + 1]) {
                switchSent[currentPtr + 1] = true;
                logger.print("returning " + (optimalPathLanes.get(currentPtr + 2) - optimalPathLanes.get(currentPtr + 1)));
                return optimalPathLanes.get(currentPtr + 2) - optimalPathLanes.get(currentPtr + 1);
            } else {
                logger.print("returning 0");
                return 0;
            }
        }
        return 0;
    }

    void update(int curPieceId) {
        if (!hasStarted) {
            return;
        }
        logger.print("update: pieceId = " + curPieceId + " ptr = " + currentPtr);
        while (currentPtr + 1 < optimalPathPieces.size() && optimalPathPieces.get(currentPtr) != curPieceId) {
            currentPtr++;
            if (currentPtr < optimalPathLanes.size())
                totalDistance += prefixSums[currentPtr] - prefixSums[currentPtr - 1];
        }
    }

    final double MAGIC_CONSTANT = 6.5;
    private void calcMaxSpeed(OptimalPathFinder pathFinder) {
        maxSpeed = new double[optimalPathLanes.size()];
        for (int i = 0; i < maxSpeed.length; i++) {
            maxSpeed[i] = MAGIC_CONSTANT;
        }
        for (int i = 0; i < maxSpeed.length - 1; i++) {
            Piece piece = pieces[optimalPathPieces.get(i)];
            Lane lane = lanes[optimalPathLanes.get(i)];
            Lane lane2 = lanes[optimalPathLanes.get(i + 1)];
            if (piece.radius > 0) {
                double r = piece.radius -
                        Math.signum(piece.angle) * (lane.distanceFromCenter + lane2.distanceFromCenter) / 2;
                maxSpeed[i] = MAGIC_CONSTANT * Math.sqrt(r / 90);
            } else {
                maxSpeed[i] = 1 / friction;
            }
        }
    }

    private void calcPrefixSums(OptimalPathFinder pathFinder) {
        logger.print("Calculating prefix sums");
        prefixSums = new double[optimalPathPieces.size()];
        prefixSums[0] = 0;
        for (int i = 0; i < prefixSums.length - 1; i++) {
            int curPieceId = optimalPathPieces.get(i);
            int curLaneId = optimalPathLanes.get(i);
            int nextLaneId = optimalPathLanes.get(i + 1);
            prefixSums[i + 1] = prefixSums[i] + pathFinder.distCache[curPieceId][curLaneId][nextLaneId];
        }
        logger.print("prefixSums : " + Arrays.toString(prefixSums));
    }

    private void getOptimalPath(OptimalPathFinder pathFinder, int[] optimalEnds, int startLaneId) {
        currentPtr = 0;
        optimalPathPieces = new ArrayList<Integer>();
        optimalPathLanes = new ArrayList<Integer>();
        int curLaneId = startLaneId;
        for (int lap = 0; lap < laps; lap++) {
            int pieceId = 0;
            while (true) {
                int next = pathFinder.getNext(pieceId, curLaneId, optimalEnds[lap]);
                if (next == -2) {
                    break;
                }
                optimalPathPieces.add(pieceId);
                optimalPathLanes.add(curLaneId);
                pieceId++;
                curLaneId += next;
            }
        }
        //optimalPathPieces.add(0);
        optimalPathLanes.add(optimalEnds[laps - 1]);
        switchSent = new boolean[optimalPathLanes.size()];
        Arrays.fill(switchSent, false);
        String optimalPath = "\n";
        for (int i = 0; i < optimalPathPieces.size(); i++) {
            optimalPath += "(" + optimalPathPieces.get(i) + ", " + optimalPathLanes.get(i) + ")\n";
        }
        logger.print("optimal path:" + optimalPath);
    }

    public static final double BAD_ANGLE = 15.0;

    private double getThrottle(CarData ourData, int gameTick, OptimalPathFinder pathFinder) {
        logger.print("getThrottle: pieceId = " + ourData.piecePosition.pieceIndex + " gameTick = " + gameTick);
        // HERE IS SOME FUCKING MAGIC
        if (gameTick < 3 || !hasStarted) {
            return 1.0;
        }
        int nextTurn = findNextTurn(currentPtr);
        if (nextTurn == -1) {
            return calcSpeed(speed, maxSpeed[currentPtr]);
        }
        double speedLocal = maxSpeed[currentPtr];
//        if ((Math.abs(ourData.angle) < BAD_ANGLE || ourData.angle * pieces[optimalPathPieces.get(currentPtr)].angle <= 0)
//                && Math.abs(lastAngle - ourData.angle) < 0.5) {
//            speedLocal /= (1 - friction / mass);
//        }
//        else if (Math.abs(ourData.angle) > 2*BAD_ANGLE && ourData.angle * pieces[optimalPathPieces.get(currentPtr)].angle > 0) {
//            speedLocal = speedLocal * (1 - friction / mass);
//        }
//        logger.print("speedLocal = " + speedLocal + " angle = " + ourData.angle + " mapBendAngle = " + pieces[optimalPathPieces.get(currentPtr)].angle);
        boolean good = true;
        double speedNext = 0;
        while (nextTurn != -1 && good) {
            //logger.print("nextTurn = " + nextTurn + " maxspeed = " + maxSpeed[nextTurn]);
            double distToTurn = prefixSums[nextTurn] - prefixSums[currentPtr]
                    - ourData.piecePosition.inPieceDistance;
            boolean curGood = true;
            double x = 0.0;
            double curSpeed = speed*(1 - friction / mass) + calcSpeed(speed, speedLocal)/mass;
            x += curSpeed;
            if (x >= distToTurn && curSpeed > maxSpeed[nextTurn]) {
                good = false;
                speedNext = maxSpeed[nextTurn];
                break;
            }
            while (true) {
                curSpeed *= 1 - friction / mass;
                x += curSpeed;
                if (curSpeed < maxSpeed[nextTurn]) {
                    curGood = true;
                    break;
                }
                if (x >= distToTurn) {
                    curGood = false;
                    speedNext = maxSpeed[nextTurn];
                    break;
                }
            }
            good &= curGood;
            nextTurn = findNextTurn(nextTurn);
        }
        logger.print("gameTick = " + gameTick + " good = " + good + " speedLocal = " + speedLocal);
        if (speedNext < speed * (1-friction/mass))
            speedNext = speed * (1-friction/mass); // force possible local breaking.
        if (speedNext > speedLocal)
            speedNext = speedLocal;
        return good ? calcSpeed(speed, speedLocal) : calcSpeed(speed, speedNext);
    }

    private double calcSpeed(double speed, double maxSpeed) {
        double ret = Math.min(1.0, mass * maxSpeed - (mass - friction) * speed);
        if (ret < -OptimalPathFinder.EPS) {
            logger.print("speed = " + speed + " distSpeed = " + maxSpeed + " ret = " + ret);
            //throw new AssertionError();
        }
        if (ret < 0)
            ret = 0;
        return ret;
    }

    private int findNextTurn(int start) {
        if (!hasStarted) {
            throw new AssertionError();
        }
        for (int i = start + 1; i < optimalPathPieces.size(); i++) {
            Piece piece = pieces[optimalPathPieces.get(i)];
            if (piece.radius > 0) {
                return i;
            }
        }
        return -1;
    }

    private void calcPhysicsConstants() {
        double x1 = firstCoordinate - zeroCoordinate;
        double x2 = secondCoordinate - zeroCoordinate;
        if (x1 > 0 && x2 > x1) {
            friction = (3 * x1 - x2) / (x1 * x1);
            mass = 1 / x1;
            double k = (1 - friction / mass);
            predictedSpeed = 0;
            for (int tick = 0; tick < 2; tick++)
                predictedSpeed = predictedSpeed * k + 1 / mass;
            assert (predictedSpeed == x2 - x1);
            physicsKnown = true;
        } else {
            zeroCoordinate = firstCoordinate;
            firstCoordinate = secondCoordinate;
        }
    }

    public static void main(String... args) throws IOException, CloneNotSupportedException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String raceType = "join";
        String password = null;
        String trackName = null;
        int carCount = 0;
        if (args.length > 4) {
            raceType = args[4];
            password = args[5];
            trackName = args[6];
            carCount = Integer.parseInt(args[7]);
        }
        logger = new Logger(botName, "info.log");
        mapLogger = new Logger(botName, "map.log");
        gameLogger = new Logger(botName, "game.log");
        pathFinderLogger = new Logger(botName, "pathFinder.log");
        logger.print("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey, raceType, password, trackName, carCount);
    }


    private String getDirection(int dir) {
        logger.print(dir + "");
        return dir == -1 ? "Left" : "Right";
    }

    private CarData findCar(ArrayList<CarData> data, CarId ourCar) {
        for (CarData carData : data) {
            if (carData.id.equals(ourCar)) {
                return carData;
            }
        }
        return null;
    }


    private void send(final SendMsg msg) {
        logger.print("Sending: " + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
        logger.print("Message sent");
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class BotId {
    protected String name;
    protected String key;

    BotId(String name, String key) {
        this.name = name;
        this.key = key;
    }
}

class SwitchLane extends SendMsg {
    protected String direction;

    SwitchLane(String direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class JoinRace extends SendMsg {
    protected BotId botId;
    protected String trackName;
    protected String password;
    protected int carCount;

    JoinRace(BotId botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class CreateRace extends SendMsg {
    protected BotId botId;
    protected String trackName;
    protected String password;
    protected int carCount;

    CreateRace(BotId botId, String trackName, String password, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}