package rev20140420;

import java.util.ArrayList;

class PieceLane {
    protected int startLaneIndex;
    protected int endLaneIndex;

    PieceLane(int startLaneIndex, int endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }
}

class PiecePosition {
    protected int pieceIndex;
    protected double inPieceDistance;
    protected PieceLane lane;
    protected int lap;

    PiecePosition(int pieceIndex, double inPieceDistance, PieceLane lane, int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }
}

class CarData {
    protected CarId id;
    protected double angle;
    protected PiecePosition piecePosition;

    CarData(CarId id, double angle, PiecePosition piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }
}

public class CarPositionsWrapper {
    protected String msgType;
    protected ArrayList<CarData> data;
    protected String gameId;
    protected int gameTick;

    public CarPositionsWrapper(String msgType, ArrayList<CarData> data, String gameId, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }
}
