max angle linear from speed (startng from speed > speededge).



x'(0) = v;                                              
x(0) = 0;
m - mass; a - friction coefficient; T - acceleration force(=throttle, because we can scale a and m, to remove all coeff near T)

x(t) = (e^(-(a t)/m) (a t T e^((a t)/m)-m (e^((a t)/m)-1) (T-a v)))/a^2

x'(t) = -(T e^(-(a t)/m))/a+v e^(-(a t)/m)+T/a 

breaking (T=0):
x(0) = 0;
x'(0) = v;

what time in advance and distance should you start breaking (T=0)?

x(t) = (m v (1-e^((-a t)/m)))/a

x'(t) = v e^((-a t)/m)

x'(t) = v2 < v;

v2 = v e^((-a t)/m)
e^((-a t)/m) = v2/v

a*t/m = ln(v0)-ln(v2)

Breaking duration: t = m/a*(ln(v0)-ln(v2))
Breaking length: l = x(t) = (m (v0-v2))/a
Throttle to maintain speed at v2: T = a*v2

To estimate all parameters, measure:
k - coefficient of speed decrease for tau seconds of 0 throttle (not fully stopping!)
V - max reached speed

a = 1/V
m = a*tau/ln(k)
 
or while accelerating for tau seconds endng with speed v2:

m = a*tau/ln(V/(V-v2))


