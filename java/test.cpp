#include <iostream>
#include <cmath>
#include <cstdio>


using namespace std;

/*
v = 5.5 R = 90.              v=6                              v=6.5                          
0.0 					     0.0               				  0.0                            
0.04102640798422155          0.2376168857048403               0.4118468012341516             
0.11869452659934566          0.66215113594622                 1.1910094683190129             
0.2288062164667855           1.2517270073936744               2.2944257179994483             
0.3673601025934887           1.9854185188907403               3.680704934987247              
0.5305594073864076           2.843311420096229                5.3102973039136465             
0.7148175937584691           3.806551375280256                7.14563107158726               
0.9167619985204506           4.857379379381088                9.151220011271018              
1.1332356320506212           5.979155417476289                11.293743195628977             
1.3612973152416419           7.156371365880706                13.542099199320806             
1.5982203190554891           8.374654113950387                15.867436847883122             
1.8414896657786597          9.62075986110828                 18.24316460843431              
2.088798244361499            10.88256051434188               20.644940681721               
2.3380418851402833           12.149023078144367               23.05064580587419             
2.5873135318650813           13.410182892230333               25.440340721673486             
2.8348966413700407           14.657111532965784               27.79621017876341             
3.0792579334992984           15.881880152880257               30.102495283676042            
3.319039606107036            17.07751898940649                32.3454159051517                                 
3.5530511221462278           18.237973729609358               34.51308476148458                                
3.7802606681009583           19.3580593725697                 36.59541471973126                                
3.9997863753512353           20.43341218568954                38.58402073878961                                
4.210887388530159            21.46044030585452                40.472117788673614                               
4.412954857579258          22.436273491458916               42.2544159777704                                 
4.605502923061804            23.35871248706672                43.92701401937227                                
4.788159757384293            24.226178419210576              45.48729206914071                                
4.960658717926728            25.037662599745794               46.93380486710468                                
5.122829661713387            25.792677072479183               48.26617602196119                                
5.274590465181277            26.49120619964543                49.484994182387766                               
5.4159387868384705           27.133659547347538               50.59171175027394                                
5.546944105154677            27.720826293424118               51.588546704634666                               
5.667740058900536            28.253831347442187               52.478388022818315                               
5.77851711235108             28.734093340702422               53.264705107732354                               
5.879515563293368            29.163284614331193               53.951461556388814                               
5.971018907627997            29.543293305739432               54.543033536268126
6.0533475705234325           29.87618760796359                55.04413297191148 
6.126853010566188            30.164182252655433               55.45973568482787 
6.191912200141236            30.409607245732964               55.79501457524754 
6.248922482367055            30.61487886490957                56.05527788443552 
6.29829680228823             30.782472910431558               56.24591253112781 
6.340459307685767            30.914900184322935               56.372332475069605 
6.375841312787421            31.01468415919254                56.43993202449144 
6.404877616337742            31.084340785131065               56.45404397250625 
6.428003163904918            31.126360372337043               56.419902419677115
6.445650042947741            31.14319147777972 
6.458244798025226            31.137226716344607
6.466206052592749            31.11079041043021 
6.469942423076156            31.066127986778845
6.469850710336783            31.0053970253416  
6.466314353222026                               
6.459702128624554 
6.450367082336744 


















*/

int n;

double len[40];
double   R[40];
double turn[40];

long double F=0.9, A=-0.00125, K=-0.3, L=0.530330086;
// da' = F*da + A*v*a + sign(turn)*max(0, K*v+L*v*v/sqrt(R));
// a+=da;
long double B = 0.98, M = 0.2;
long double Mass = 5, Friction = 0.1;
// v = B*v+M*T
// x += v;


const long double BAD_ANGLE = 60;

class State
{
	public:
	 long double a, da, x, v;

	State()
	{
		a = da = x = v = 0;
	}
	State(long double _a, long double _da, long double _x, long double _v) {
		a = _a; da = _da; x = _x; v = _v;
	}
	
	State next(long double tr, long double R, long double alpha) {
		State t(a,da,x,v);
		long double diff = 0;
		if (R > 0) diff = K*v+L*v*v/sqrt(R);
		if (diff < 1e-9) diff = 0;
		if (alpha < 0) diff = -diff;
		t.da = F*t.da+A*a*v+diff;
		t.a += t.da;
		t.v = B*t.v + M*tr;
		t.x += t.v;
		return t;
	}
};


long double CriticalSpeed(int i, long double x)
{
	int j;
	long double ans = -K*sqrt(R[i])/L;
	if (R[i]== 0) ans = 1000;
	for (j = n-1; j > i; j--) {
		long double cur = -K*sqrt(R[j])/L;
		if (R[j]== 0) cur = 1000;
		long double dist = len[i] - x;
		for (int k = i+1; k < j; k++) dist += len[k];
		cur += Friction / Mass * dist;
		if (cur < ans) ans = cur;
	}
	return ans;
}


bool Can(State cur, int i, long double t0)
{
	State st = cur.next(t0, R[i], turn[i]);
	while (i < n) {
		if (st.x > len[i]) {
			st.x -= len[i];
			i++;
		}
//		cout << i << '\t' << (double)st.x << '\t' << (double)st.v << '\t' <<(double) st.a << '\t' <<(double) st.da << '\n';
		if (fabs(st.a) > BAD_ANGLE) return false;
//		cout << "CS = " << (double)CriticalSpeed(i, st.x) << "\n";
		st = st.next(max((long double)0, min(CriticalSpeed(i, st.x)*Mass-st.v*(Mass-Friction), (long double)1.0)), R[i], turn[i]);
	}
	return true;
}

const long double PI = 3.1415926535897932384626433832795;



void Load()
{
	int i;
	freopen("map.txt", "rt", stdin);
	cin >> n;
	for (i = 0; i < n; i++) {
		cin >> len[i] >> R[i] >> turn[i];
		if (len[i] < 1) {
			len[i] = PI / 180.0 * fabs(turn[i])*R[i];
		}
		cerr << (double)len[i] << ' ' << (double) R[i] << ' '<< (double)turn[i] << "\n";
	}
}

                                
int main()
{
	Load();
	State cur;
	int i = 0;
	long double throttle = 0.0;
	cout.precision(6);
	cout.setf(ios::fixed | ios::showpoint);
	cur.v = 8.0;
	int tick = 0;


	while (i < n) {
		long double a, b;
		a = max((long double)0, min(CriticalSpeed(i, cur.x)*Mass-cur.v*(Mass-Friction), (long double)1.0));
		b = max((long double)0, min(CriticalSpeed(i, cur.x)*1.2*Mass-cur.v*(Mass-Friction), (long double)1.0));
		for (int it = 0; it < 40; it++) {
			long double m = (a+b)/2.0;
			if (Can(cur, i, m))
				a = m;
			else
				b = m;
		}
		throttle = a;	
//		throttle = max((long double)0, min(6.5*Mass-cur.v*(Mass-Friction), (long double)1.0));
		cur = cur.next(throttle, R[i], turn[i]);	
		if (cur.x > len[i]) {
			cur.x -= len[i];
			i++;
		}		

		cout << i << '\t' << (double)cur.x << '\t' << (double)cur.v << '\t' <<(double) cur.a << '\t' <<(double) cur.da << '\n';
		if (fabs(cur.a) > BAD_ANGLE) break;
		tick++;
	}
	cout << tick << "\n";
	if (i < n) cout << "crashed\n";	
	return 0;
}


int main_old()
{
	int i;
	long double a;
	long double m;
	a = ((long double)3*0.13-0.3874)/0.13/0.13;
	m = 1/(long double)0.13;
	long double x = 0;
//	long double v = 0;
	for (i = 0; i < 40; i++) {
		long double z = 1;
		for (int j = 0; j < i+1; j++)
			z *= (1-a/m);
		x = 1 / a * (i+1) + (1-z)*m/a*(0-1/a);
		cout.precision(10);
		cout.setf(ios::fixed | ios::showpoint);
		cout << (double) x << "\n";
	}
	return 0;
}