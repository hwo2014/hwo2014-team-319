Keimola:
lane dist:20

3 { length = 100.0, radius = 0.0, angle = 0.0, switch = true}
fixed dist of 3 from 102.02531021234056                      
fixed dist of 3 to 102.06027499293373 by 0.034964780593170985

8 { length = 0.0, radius = 200.0, angle = 22.5, switch = true}
start radius: 190
end radius: 210
fixed dist of 8 from 80.99823376756599                     
fixed dist of 8 to 81.05465220637488 by 0.05641843880888864

18 { length = 100.0, radius = 0.0, angle = 0.0, switch = true}
fixed dist of 18 from 102.02531021234056                     
fixed dist of 18 to 102.06027499293369 by 0.03496478059312391

Germany:
lane dist: 20

1 { length = 100.0, radius = 0.0 }
fixed dist of 1 from 102.02531021234056                    
fixed dist of 1 to 102.0602749929335 by 0.03496478059294894

14 { length = 100.0, radius = 0.0, angle = 0.0, switch = true}
fixed dist of 14 from 102.02531021234056                     
fixed dist of 14 to 102.06027499293383 by 0.03496478059327135

27 { length = 100.0, radius = 0.0, angle = 0.0, switch = true}
fixed dist of 27 from 102.02531021234056                     
fixed dist of 27 to 102.06027499293378 by 0.03496478059322161

36 { length = 100.0, radius = 0.0, angle = 0.0, switch = true}
fixed dist of 36 from 102.02531021234056                     
fixed dist of 36 to 102.06027499293377 by 0.03496478059321273

France:
lane dist: 20

2 { length = 0.0, radius = 200.0, angle = 22.5, switch = true}
start radius: 210
end radius: 190
fixed dist of 2 from 80.99823376756599                     
fixed dist of 2 to 81.05390415930518 by 0.05567039173918609

7 { length = 0.0, radius = 100.0, angle = 45.0, switch = true}
start radius: 90
end radius: 110
fixed dist of 7 from 81.0040653201669                      
fixed dist of 7 to 81.02948414200807 by 0.02541882184117128

17 { length = 0.0, radius = 200.0, angle = -22.5, switch = true}
start radius: 190
end radius: 210
fixed dist of 17 from 80.99823376756599                     
fixed dist of 17 to 81.05465220637497 by 0.05641843880897124

24 { length = 99.0, radius = 0.0, angle = 0.0, switch = true}
fixed dist of 24 from 101.0519113904411                   
fixed dist of 24 to 101.0804661468 by 0.028554756358893485

30 { length = 0.0, radius = 200.0, angle = 22.5, switch = true}
start raius: 210
end radius: 190
fixed dist of 30 from 80.99823376756599                      
fixed dist of 30 to 81.05390415930538 by 0.055670391739377045

USA:
lane dist: 20

1 { length = 100.0, radius = 0.0, angle = 0.0, switch = true}
fixed dist of 1 from 102.02531021234056                      
fixed dist of 1 to 102.06027499293376 by 0.03496478059320296 

7 { length = 100.0, radius = 0.0, angle = 0.0, switch = true}
fixed dist of 7 from 102.02531021234056                     
fixed dist of 7 to 102.06027499293381 by 0.03496478059325625




